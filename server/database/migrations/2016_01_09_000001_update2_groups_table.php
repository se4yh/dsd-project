<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Update2GroupsTable extends Migration
{

    public function up()
    {
        Schema::table('groups', function ($table) {
            $table->string('type');
        });
    }
}