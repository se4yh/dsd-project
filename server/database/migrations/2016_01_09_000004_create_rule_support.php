<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateRuleSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rules', function($table)
        {
            $table->dropColumn(['user_id','data','device_id']);
        });

        Schema::table('rules', function ($table) {
            $table->string('device_type');
            $table->text('text');
        });

        Schema::create('user_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rule_id');
            $table->integer('user_id');
            $table->text('serialized_data');
            $table->timestamps();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('property_rule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rule_id');
            $table->integer('property_id');
            $table->timestamps();
        });

        Schema::create('user_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_rule_id');
            $table->integer('property_id');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_rules');
        Schema::drop('properties');
        Schema::drop('property_rule');
        Schema::drop('user_properties');
    }
}
