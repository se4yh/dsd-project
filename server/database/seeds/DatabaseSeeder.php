<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call('GroupsSeeder');
        $this->call('UsersSeeder');
        $this->call('DevicesSeeder');
        $this->call('DeviceGroupSeeder');
        $this->call('TokensSeeder');
    }
}


class GroupsSeeder extends Seeder
{
    public function run()
    {
        DB::table('groups')->delete();

        $groups=[
            array(
                'name' => 'room',
                'info' => 'some room',
                'id' => 1
            ),array(
                'name' => 'kitchen',
                'info' => 'where the fridge is...',
                'id' => 2
            )
        ];
        DB::table('groups')->insert($groups);
    }
}


class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert(
            array(
                'name' => 'test',
                'email' => 'test@test.com',
                'password' => Hash::make('test'),
                'id'=>'1'
            )
        );
    }
}

class DevicesSeeder extends Seeder
{
    public function run()
    {
        DB::table('devices')->delete();

        $devices=[
            array(
                'name' => 'living_room_heater',
                'type' => 'heater',
                'id' => '1'
            ),
            array(
                'name' => 'living_room_ceil',
                'type' => 'light',
                'id' => '2'
            ),
            array(
                'name' => 'living_room_table',
                'type' => 'light',
                'id' => '3'
            ),
            array(
                'name' => 'living_room_ac',
                'type' => 'air_conditioner',
                'id' => '4'
            )
        ];
        DB::table('devices')->insert($devices);
    }
}

class DeviceGroupSeeder extends Seeder
{
    public function run()
    {
        DB::table('device_group')->delete();

        $devicegroup=[
            array(
                'group_id' => 2,
                'device_id' => 1
            ),
            array(
                'group_id' => 2,
                'device_id' => 2
            ),
            array(
                'group_id' => 2,
                'device_id' => 3
            ),
            array(
                'group_id' => 1,
                'device_id' => 1
            ),
            array(
                'group_id' => 1,
                'device_id' => 3
            )
        ];

        DB::table('device_group')->insert($devicegroup);
    }
}

class TokensSeeder extends Seeder
{
    public function run()
    {
        DB::table('tokens')->delete();

        DB::table('tokens')->insert(
            array(
                'user_id' => 1,
                'token' => "a33a137aa3d228aed47453fe6e2e0fc1547d901c1b70d612",
                'expires' => "2023-12-08 20:45:59"
            )
        );
    }
}