<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7.12.2015.
 * Time: 11:47
 */
//use Illuminate\Http\Response;


class DeviceAPITest extends TestCase
{
    public function testlshowDevicedetails200(){
        $this->refreshApplication();

        $response = $this->call('get','/device/details/1',['test'=>'on']);
        $json_response = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode()); //right status code?

        $this->assertTrue(isset($json_response->device),"device details json response doesn't have device object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->device->id),"device details json response doesn't have id object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->device->name),"device details json response doesn't have name object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->device->type),"device details json response doesn't have type object!");  // devices object exist in json response?
        //$this->assertTrue(isset($json_response->device->state),"device details json response doesn't have state object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->device->groups),"device details json response doesn't have groups object!");  // devices object exist in json response?
    }
}