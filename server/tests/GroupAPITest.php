<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7.12.2015.
 * Time: 9:48
 */

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use \Illuminate\Support\Facades\Log;

class GroupAPITest extends TestCase
{

    private $group_id=1;
    private $device_id=3;

    public function testGroupAssign200()
    {
        $group = \App\Models\Group::where('id','=',$this->group_id)->first();
        $group->devices()->detach($this->device_id);
        $response=$this->post('/groups/assign', ['test' => 'sarma','group_id'=>$this->group_id,'device_id'=>$this->device_id])
            ->seeJson([
                'message' => 'device added'
            ]);

        $this->assertEquals($response->response->getStatusCode(), 200);
    }

    public function testGroupAssignDuplicateError(){
        //expect duplicate error
        $response=$this->post('/groups/assign', ['test' => 'sarma','group_id'=>$this->group_id,'device_id'=>$this->device_id])
            ->seeJson([
                'message' => 'device already in group'
            ]);
        $this->assertEquals($response->response->getStatusCode(), 404);
    }

    public function testGroupAssignGeneralError(){
        $response=$this->post('/groups/assign', ['test' => 'sarma','device_id'=>$this->device_id])
            ->seeJson([
                'message' => 'device not added'
            ]);

        $this->assertEquals($response->response->getStatusCode(), 404);
    }

    //create group
    public function testGroupAddNew200()
    {
        $response=$this->post('/group/addnew', ['name' => 'sarma_room','description'=>'bla bla','test'=>'sarma'])
            ->seeJson([
                'message' => 'group added'
            ]);
       // \Illuminate\Support\Facades\Log::info($response->response->getContent());

        $this->assertEquals($response->response->getStatusCode(), 200);
    }

    //delete group
    public function testGroupDelete200()
    {
        $this->refreshApplication();
        $nova_grupa=\App\Models\Group::where('name','sarma_room')->first()->id;
        $response = $this->call('post','/group/delete/',['test'=>'sarma','ids'=>[$nova_grupa,999]],[],[],[],[]);
       // \Illuminate\Support\Facades\Log::info($response->getContent());
        $json_response = json_decode($response->getContent());
        //var_dump($nova_grupa);


        $this->assertTrue(isset($json_response->message));

        $this->assertEquals($response->getStatusCode(), 200); //right status code?
    }

    public function testGroupsShowall200()
    {
        $response = $this->call('get','/groups/all',['test' => 'sarma'],[],[],[],[]);

        $output = new Symfony\Component\Console\Output\ConsoleOutput();
        $output->writeln("<info>$response</info>");
        $json_response = json_decode($response->getContent());

        $this->assertTrue(isset($json_response->groups),"devices-all json response doesn't have groups object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->groups[0]->id),"devices-all json response doesn't have id object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->groups[0]->name),"devices-all json response doesn't have name object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->groups[0]->description),"devices-all json response doesn't have description object!");  // devices object exist in json response?

        $this->assertEquals($response->getStatusCode(), 200);
    }
}