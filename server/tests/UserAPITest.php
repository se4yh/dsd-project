<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7.12.2015.
 * Time: 11:47
 */
use Illuminate\Http\Response;

$global_token; //globals - best practice :(

class UserAPITest extends TestCase
{

    protected $user_name = "test@test.com";
    protected $user_password = "test";

    public $response;

    function __construct()
    {
        $this->response = new Response();
    }

    public function testlogin200(){
        global $global_token;
        $response = $this->post('/authentication/login', ["username" => "test@test.com", "password" => "test"])
            ->seeJson([
                'user' => ["id" => 1, "name" => "test", "email" => "test@test.com"]
            ]);

        $global_token = json_decode($response->response->getContent())->token;
        $this->assertEquals($this->user_name, \App\Models\Token::where('token', $global_token)->first()->user->email);

        $this->assertEquals($response->response->getStatusCode(), 200);
    }

    public function testlogin401(){
        $response = $this->post('/authentication/login', ["username" => "test@test.com", "password" => "testwrongggg"])
            ->seeJson([
                'message' => "Wrong username or password"
            ]);

        $this->assertEquals($response->response->getStatusCode(), 401);
    }

    public function testlogout200(){
        global $global_token;
        $this->refreshApplication();
        $response = $this->get('/authentication/logout', ['X-AccessToken'=>$global_token])
            ->seeJson([
                'message' => "logout success"
            ]);


        $this->assertEquals($response->response->getStatusCode(), 200);
    }

}