<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 7.12.2015.
 * Time: 11:47
 */
//use Illuminate\Http\Response;


class DevicesAPITest extends TestCase
{


    public function testlshowDevicesAll200(){
        $this->refreshApplication();
       // $response = $this->get('devices/all',[]);
        $response = $this->call('get','/devices/all',['test'=>'sarma'],[],[],[],[]);

        $json_response = json_decode($response->getContent());

        $this->assertTrue(isset($json_response->devices),"devices-all json response doesn't have devices object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->devices[0]->id),"devices-all json response doesn't have id object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->devices[0]->name),"devices-all json response doesn't have name object!");  // devices object exist in json response?
        $this->assertTrue(isset($json_response->devices[0]->type),"devices-all json response doesn't have type object!");  // devices object exist in json response?
        $this->assertEquals($response->getStatusCode(), 200); //right status code?
    }




}