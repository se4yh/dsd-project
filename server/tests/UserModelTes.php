<?php

/**
 * Created by PhpStorm.
 * User: Elena
 * Date: 11/26/2015
 * Time: 6:17 PM
 */
class UserModelTest extends TestCase
{

    public function testUserModel()
    {
       // $userTest = factory(App\Models\User::class, 3)->make();
       // dd($userTest -> toArray());
    }

    public function testLogout()
    {
        Auth::shouldReceive('logout')->once()->withNoArgs()->andReturn();

        $response = $this->call('POST', '/authentication/logout', [
            '_token'   => csrf_token()
        ]);

        //print_r($response->getContent());

        $this->assertResponseStatus(200);
    }
}