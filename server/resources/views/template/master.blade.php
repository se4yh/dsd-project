<html>

<head>
    <meta charset="utf-8">
    <title>@yield('title', 'SSP')</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <link href="{{asset('inc/css/bootstrap-switch.min.css')}}" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="{{asset('inc/js/bootstrap-switch.min.js')}}"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="row-fluid">
    <div class="span12 well">
        <h1>Simulators </h1>  <button type="button" class="btn btn-success" onclick="monitor(this)" > Start monitoring</button>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <ul class="nav nav-list well">

        </ul>
    </div>
    <div class="span9">
        @yield('content')
    </div>
</div>
</body>
</html>

<style>
    #kasli{
        position:absolute;
        top:10px;
        left:90%;
    }
</style>