@extends('template.master')
@section('title')
    simulation
@stop


@section('content')
   <div class="row" style="padding: 5px;">
        <div class="col-md-offset-2 col-md-8">
            @if(count($devices))
                <div class="list-group">
                    @foreach($devices as $device)
                        <h3>{{$device->getName()}}</h3>
                        <h4>type: {{$device->getType()}}</h4>
                        <form class="form-horizontal" role="form" id="form{{$device->getID()}}">
                            <?php //dd($device->getItems());?>
                            @foreach($device->getItems() as $item)
                                <div class="form-group">
                                    <label for="inputEmail1" class="col-md-2 control-label">{{$item["name"]}}<br> <?php echo(($item["editable"]=="YES")?'':'<small>(readonly)</small>'); ?></label>
                                    <div class="col-md-10" style="height: 35px;">
                                        @if($item["type"]=="SWITCH")
                                            <input type="checkbox" name="{{$item["name"]}}" <?php echo(($item["value"]=="ON")?'checked':''); ?> >
                                            <script>
                                                $("[name='{{$item["name"]}}']").bootstrapSwitch();
                                            </script>
                                        @elseif($item["type"]=="CONTACT")
                                            <select name="{{$item["name"]}}" class="form-control">
                                                <option value="OPEN" {{($item["value"]=="OPEN")?'selected':''}}>OPEN</option>
                                                <option value="CLOSED"  {{($item["value"]=="CLOSED")?'selected':''}} >CLOSED</option>
                                            </select>
                                        @else
                                            <input type="text" name="{{$item["name"]}}" value="{{$item["value"]}}" class="form-control" id="inputEmail1">
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="button" class="btn btn-info refreshButton" onclick="{{"AjaxRefresh(".$device->getID().')'}}">Refresh device</button>
                                    <button type="button" class="btn btn-danger pull-right" onclick="{{"AjaxAction(".$device->getID().')'}}">Apply Actions</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                        <hr>
                    @endforeach
                </div>
                <script>
                    function AjaxAction(device)
                    {



                        $('#form'+device).serializeArray();
                        var url = "<?php echo(URL::route('sim-action',''));?>/"+device+"?test=sarma"; // the script where you handle the form input.

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $('#form'+device).serializeArray(), // serializes the form's elements.
                            success: function (data) {
                                console.log(data); // show response from the php script.
                            }
                        });

                    }

                    function AjaxRefresh(device)
                    {


                        console.log(device);
                        $('#form'+device).serializeArray();
                        var url = "<?php echo(URL::route('sim-refresh',''));?>/"+device+"?test=sarma"; // the script where you handle the form input.

                        $.ajax({
                            type: "GET",
                            url: url,
                            data: $('#form'+device).serializeArray(), // serializes the form's elements.
                            success: function (data) {
                                items= JSON.parse(data);
                                for (var key in items){
                                    //console.log(key);
                                    //console.log(items[key].value);
                                    var inputObj =$('#form'+device+' [name="'+key+'"]');
                                    //console.log(inputObj.attr('type'));
                                    console.log(inputObj.val());
                                    if(inputObj.attr('type')=='checkbox'){
                                        if(items[key].value == 'ON'){
                                            //inputObj.attr('checked', true);
                                            //inputObj[0].checked = true;
                                            inputObj.bootstrapSwitch('state', true, true);
                                        }else{
                                            //inputObj.attr('checked', false);
                                            //inputObj[0].checked = false;
                                            inputObj.bootstrapSwitch('state', false, false);
                                        }
                                    }else {
                                        inputObj.val(items[key].value);
                                    }
                                    console.log(inputObj.val());
                                }
                            }
                        });

                    }

                    var monitorEnable=false;

                    function monitor(btn){
                       //debugger;
                        if ($(btn).hasClass('btn-success')){
                            console.log("start monitoring");
                            $(btn).removeClass('btn-success');
                            $(btn).addClass('btn-danger');
                            $(btn).html('Stop monitoring');
                            monitorEnable=true;
                        }else{
                            console.log("stop monitoring");
                            $(btn).removeClass('btn-danger');
                            $(btn).addClass('btn-success');
                            $(btn).html('Start monitoring');
                            monitorEnable=false;
                        }
                    }

                    function timeout() {
                        setTimeout(function () {
                            if (monitorEnable) {
                                reload();
                            }
                            timeout();
                        }, 1000); // Adjust the timeout value as you like
                    }
                    function reload(){
                        var btns =$('.refreshButton');
                        console.log( btns );

                        btns.each(function(){
                            $(this).click();
                        });

                        console.log( "devices refresh!" );
                    }

                    $( document ).ready(function() {
                        console.log( "ready!" );
                        timeout();
                    });

                </script>
            @else
                No devices :(
            @endif
        </div>
    </div>

@stop