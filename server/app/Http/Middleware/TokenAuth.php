<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Carbon\Carbon;
use Closure;
use Auth;
use Illuminate\Support\Facades\Input;
use Response;
use Request;

// use Illuminate\Contracts\Auth\Guard;

class TokenAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //try to authorize
        //echo("tu");
        //die();
        //var_dump(Token::where('token',Input::get('token'))->first()->user);

        /*this is for easier testing from brovser, will be removed for release*/
        if(Input::has('test')){
            Auth::loginUsingId(1);
            return $next($request);
        }

        try {
            $token=Token::where('token','=', Request::header('x-accesstoken'))->with('user')->first();

            //check if token has expired
            $expires=Carbon::createFromFormat('Y-m-d H:i:s',$token->expires);
            if(Carbon::now()->gt($expires))throw new \exception;

            //authenticate user
            Auth::loginUsingId($token->user->id);

        }catch(\Exception $e){
            return Response::json(array( //E001
                'message' => "Unauthorized access",
                'code' => 'E000'
            ),
                401
            );
        }

        return $next($request);
    }
}
