<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* ROUTES WITHOUT AUTH NEEDED  */
Route::get('/', function () {
    //var_dump(\App\Models\Group::find(3)->devices->first()->name);
    return view('welcome');
});

Route::post('authentication/login', array(
    'as' => 'user-login',
    'uses' => 'UserController@userLogin'
));

Route::get('/rules/test/{rId}', array(
    'as' => 'rule-t',
    'uses' => 'RuleController@eraseRuleInOH'
));


/* AUTH REQIRED ROUTES */
Route::group(array('middleware' => 'tokenAuth'), function(){

    /*********************** USER ROUTES ********/
    Route::get('/authentication/logout', array(
        'as'  => 'user-logout',
        'uses' => 'UserController@userLogout'
    ));


    /*********************** GROUP ROUTES ********/

    Route::post('/group/addnew', array(
        'as' => 'group-new',
        'uses' => 'GroupController@addNewGroup'
    ));

    Route::get('/groups/all', array(
        'as' => 'group-all',
        'uses' => 'GroupController@showGroupsAll'
    ));

    Route::post('/groups/assign', array(
        'as' => 'group-assign',
        'uses' => 'GroupController@groupsAssign'
    ));

    Route::post('/group/delete', array(
        'as' => 'group-delete',
        'uses' => 'GroupController@deleteGroup'
    ));


    /*********************** DEVICES ROUTES ********/

    Route::get('/devices/{groupId}', array(
        'as' => 'device-all',
        'uses' => 'DevicesController@showDevicesAll'
    ));

    Route::get('/refreshdevices', array(
        'as' => 'device-refesh',
        'uses' => 'DevicesController@refreshDevices'
    ));

    /*********************** DEVICE ROUTES ********/

    Route::get('/device/details/{deviceId}', array(
        'as' => 'device-details',
        'uses' => 'DeviceController@showDeviceDetails'
    ));

    Route::post('/device/action/{deviceId}', array(
        'as' => 'device-action',
        'uses' => 'DeviceController@MakeAction'
    ));

    /************************ RULES ROUTES *******/

    Route::post('/rules/addnew', array(
        'as' => 'rule-new',
        'uses' => 'RuleController@addNewRule'
    ));

    Route::get('/rules/all', array(
        'as' => 'rule-all',
        'uses' => 'RuleController@showRulesAll'
    ));

    Route::get('/rules/getrule/{ruleId}', array(
        'as' => 'rule-getrule',
        'uses' => 'RuleController@showRule'
    ));

    Route::post('/rules/delete', array(
        'as' => 'rules-delete',
        'uses' => 'RuleController@deleteRule'
    ));

    Route::get('/rules/bydevice/{deviceId}', array(
        'as' => 'rules-bydevice',
        'uses' => 'RuleController@getRuleByDeviceId'
    ));

    Route::get('/rules/properties/{ruleId}', array(
        'as' => 'rules-properties',
        'uses' => 'RuleController@getRuleProperties'
    ));



    /*********************** SIMULATION ROUTES ********/

    Route::get('/sim/devices', array(
        'as' => 'sim-devices',
        'uses' => 'SimController@showDevices'
    ));

    Route::post('/sim/action/{deviceId}', array(
        'as' => 'sim-action',
        'uses' => 'SimController@doAction'
    ));

    Route::get('/sim/refresh/{deviceId}', array(
        'as' => 'sim-refresh',
        'uses' => 'SimController@refreshDevice'
    ));

});



