<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\Token;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Routing\Controller as BaseController;
use Input;
use Auth;
use Response;
use Session;


class UserController extends BaseController {
    public function userLogin(){

        $data = (object)Input::all();

        if (Input::has(['username','password']) && Auth::attempt(['email' => $data->username, 'password' => $data->password]))
        {
            try {
                $user = Auth::user();

                $token = new Token();

                //add token to database
                $token_hash = bin2hex(openssl_random_pseudo_bytes(24));
                $token->token = $token_hash;
                $token->user_id = $user->id;
                $token->expires = Carbon::now()->addDays(365)->toDateTimeString(); // need fix - refresh token
                $token->save();

                return Response::json(array(
                    'token' => $token_hash,
                    'user' => array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email
                    )),
                    200
                );

            }catch (\Exception $e){
                return Response::json(array(
                    'message' => "internal server error",
                    'code' => "E001"
                ),
                    404
                );
            }

        }else{
            return Response::json(array(
                'message' => "Wrong username or password",
                'code' => 'E002'
               ),
                401
            );
        }
    }

    public function userLogout(){
        Auth::logout();

        //delete token from database
        try{
            Token::where('token', Request::header('x-accesstoken'))->first()->delete();
        }catch(\Exception $e){
            return Response::json(array(
                'message' => 'logout failed',
                'code' => 'E003'
            ),
                404
            );
        }


        Session::flush();
        return Response::json(array(
            'message' => 'logout success'
        ),
            200
        );

    }
}