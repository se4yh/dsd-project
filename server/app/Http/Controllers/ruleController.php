<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;

use App\Library\DeviceClass;
use App\Models\Device;
use App\Models\Rule;
use App\Models\User;
use App\Models\UserProperty;
use App\Models\UserRule;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Group;
use Input;
use Auth;
use Response;
use stdClass;

class RuleController extends BaseController {

    private $openHabRulesLocation = '/etc/openhab/configurations/rules/'; //usually for linux

    public function eraseRuleInOH($ruleId){

        //user rule
        $userRule = UserRule::find($ruleId);
        $device = new DeviceClass($userRule->device_id);
        //find the rule text in OHrules folder
        $OHLoc=env("OPENHAB_LOC",$this->openHabRulesLocation);
        //d("tu");


        $file_data = file_get_contents($OHLoc.$device->GetName().".rules");

        //find and delete
        //dd($file_data);
       // $matches=[];
       // preg_match("/\/\/s".$userRule->rule->id."rule(.+?)\/\/e".$userRule->rule->id."rule/s",$file_data,$matches);
       // dd ($matches);

        $file_data = preg_replace("/\/\/s".$userRule->rule->id."rule(.+?)\/\/e".$userRule->rule->id."rule/s","",$file_data,1);

        file_put_contents($OHLoc.$device->GetName().".rules",$file_data);

        return;
    }

    public function createRule($ruleId=null){

        //user rule
        $userRule = UserRule::find($ruleId);

        // device for wichi is this rule
        //$device = new DeviceClass($userRule->rule->device()->id);
        //$device->FetchItems();
        $device = new DeviceClass($userRule->device_id);

        //user rule properties
        $userProperties=$userRule->properties;
        //dd($properties[0]->property);
        //dd($userRule->rule->text);
        $matches=[];
        preg_match("/\|\|\|(.+?)\|\|\|/",$userRule->rule->text,$matches);
       while (count($matches)>0){
           $data=explode("__",$matches[1]);
            $replaceString='';
           switch($data[0]){
               case "userRule":
                    $replaceString=$userRule->$data[1];
                   break;
               case "property":
                   foreach($userProperties as $property)
                   {
                        if($property->property_id==$data[1]){
                            $replaceString=$property->value;
                            break;
                        }
                   }
                   break;
               case "item":
                   $replaceString=$device->GetItemName($data[1]);
                   break;
               default: break;
           }

           $userRule->rule->text = preg_replace(    "/\|\|\|(.+?)\|\|\|/",$replaceString,$userRule->rule->text,1);
           preg_match("/\|\|\|(.+?)\|\|\|/",$userRule->rule->text,$matches);
       }
        //dd($userRule->rule->text);

        //store the rule text to OHrules folder
        $OHLoc=env("OPENHAB_LOC",$this->openHabRulesLocation);

        if(file_exists($OHLoc.$device->GetName().".rules")){
            //append
            $file = fopen($OHLoc.$device->GetName().".rules", "a");
            fwrite($file,$userRule->rule->text);
            fclose($file);

        }else{
            //create and fill with template and then append
            $template_text=Rule::where("device_type","template")->first()->text;

            $file = fopen($OHLoc.$device->GetName().".rules", "w");
            fwrite($file,$template_text);
            fwrite($file,$userRule->rule->text);
            fclose($file);

        }

        return;


    }

    public function addNewRule(){

        try {

            $ruleInputData = Input::get('rule');
            $isNew=true;

            if($ruleInputData["userRuleId"] == 0){
                //dodaj novi
                $rule = new UserRule();
            }else{
                $rule = UserRule::find($ruleInputData["userRuleId"]);
                $isNew = false;
            }

            //add new user rule to database

            $rule->name = $ruleInputData["name"];

            $rule->user_id = Auth::id();
            $rule->rule_id = $ruleInputData["id"];
            $rule->device_id = $ruleInputData["deviceId"];
            $rule->save();



            //add user properties
            foreach($ruleInputData["properties"] as &$property)
            {
                if($property["userPropertyId"]==0) {
                    $userProperty = new UserProperty();
                }else{
                    $userProperty = UserProperty::find($property["userPropertyId"]);
                }
                $userProperty->user_rule_id = $rule->id;
                $userProperty->property_id =$property["id"];
                $userProperty->value =$property["value"];
                $userProperty->save();
                $property["userPropertyId"]=$userProperty->id;

            }



            //store serialized data
            $ruleInputData["userRuleId"]=$rule->id;
            $rule->serialized_data = serialize($ruleInputData);
            $rule->save();

            //delete od rule
            if(!$isNew){
                $this->eraseRuleInOH($rule->id);
            }


            //apply rule to openHAB
            $this->createRule($rule->id);

            return Response::json(array(
                'message' => "rule added"
            ),
                200
            );

        }catch (\Exception $e){
            dd($e);
            return Response::json(array(
                'message' => "Can't add new group",
                'code' => "E102"
            ),
                404
            );
        }
    }

    public function showRulesAll(){

        try {
            //fetch all rules that belongs to user
            $rules = UserRule::where('user_id',Auth::id())->get();
            //define return array
            $returnRules= new stdClass();
            $returnRules->rules= array();
            foreach($rules as $rule){
                $temp = new stdClass;
                $temp->id = $rule->id;
                $temp->ruleName = $rule->name;
                $temp->deviceName = $rule->device->name;
                $returnRules->rules[]=$temp;
            }
            return Response::json(
                $returnRules,
                200
            );
        }catch (\Exception $e){
            dd($e);
            return Response::json(array(
                'message' => "internal server error",
                'code' => "E"
            ),
                404
            );
        }

    }

    public function deleteRule(){
        try {
            if(!Input::has('ids')) throw new \Exception;
            $rule_ids = Input::get('ids');
            foreach ($rule_ids as $rule_id) {
                $this->eraseRuleInOH($rule_id);
                UserRule::destroy($rule_id);
            }

            return Response::json(array(
                'message' => "rules deleted"
            ),
                200
            );
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Cannot delete groups",
                'code' => "E106"
            ),
                404
            );
        }
    }

    public function getRuleByDeviceId($deviceId){
        try {
            $device = new DeviceClass($deviceId);
            $deviceRules = Rule::where('device_type',$device->GetType())->get();

            $returnJson = new stdClass();
            $returnJson->rules = [];

            foreach($deviceRules as $deviceRule)
            {
                $tmp = new stdClass();
                $tmp->id = $deviceRule->id;
                $tmp->name = $deviceRule->name;
                $returnJson->rules[] = $tmp;
            }



            return Response::json(
                $returnJson,
                200
            );
        }catch (\Exception $e){
            dd($e);
            return Response::json(array(
                'message' => "",
                'code' => "E"
            ),
                404
            );
        }
    }


    public function getRuleProperties($ruleId){
        try {
            $ruleProperties = Rule::find($ruleId)->properties;

            $returnJson = new stdClass();
            $returnJson->properties = [];

            foreach($ruleProperties as $ruleProperty)
            {
                $tmp = new stdClass();
                $tmp->id = $ruleProperty->id;
                $tmp->name = $ruleProperty->name;
                $tmp->type = $ruleProperty->type;
                $returnJson->properties[] = $tmp;
            }

            return Response::json(
                $returnJson,
                200
            );
        }catch (\Exception $e){
            dd($e);
            return Response::json(array(
                'message' => "",
                'code' => "E"
            ),
                404
            );
        }
    }

    public function showRule($userRuleId)
    {
        try {
            $userRule = UserRule::find($userRuleId);

            $returnJson = new stdClass();
            $returnJson->rule = new stdClass();

            $returnJson->rule = unserialize($userRule->serialized_data);

            return Response::json(
                $returnJson,
                200
            );
        }catch (\Exception $e){
            dd($e);
            return Response::json(array(
                'message' => "",
                'code' => "E"
            ),
                404
            );
        }
    }



}