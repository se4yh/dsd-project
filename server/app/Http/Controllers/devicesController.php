<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;


use App\Models\Device;
use App\Models\Group;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Response;
use URL;
require_once app_path().'/Library/Options.php';
//use App\Models\Device;

class DevicesController extends BaseController {


    public function showDevicesAll($groupID){
        try{
            //get all devices from database - TO DO -make better solution than fetching all devices,some count function
            $devices = Device::all();


            //if there is no device in database , try to ferch them from network (firs time)
            if($devices->count() == 0){
                 // call function refresh devices - return same , yust make OH req first
                 $this->refreshDevices();
            }else{
                 $response_json =   $this->getListOfDevicesJson($groupID);
                  return Response::json(
                      $response_json,
                      200
                  );
            }
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Cannot return devices from database",
                'code' => 'E203'
            ),
                404
            );
        }



    }


    public function refreshDevices(){
        $agent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
        $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'agent'  => $agent,
                    'header' => "Content-Type: application/json\r\n"
                )
            )
        );

        //make rest request
        try {
            $json_content = json_decode(file_get_contents("http://localhost:8080/rest/items?type=json", false, $context));
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "can't communicate with OpenHAB",
                'code' => 'E201'
            ),
                404
            );
        }
        try {
            //store fetched devices
            foreach ($json_content->item as $item) {

                //parse item data
                $item_data=explode("__",$item->name);
                //ignore bad items
                if(isset($item_data[2]) && in_array($item_data[1], \App\Library\getItems())){
                    //check if item already exist

                    if(!Device::where("name",$item_data[0])->count() > 0){
                        //store device
                        $newDevice = new Device();
                        $newDevice->name=$item_data[0];
                        $newDevice->type=$item_data[1];
                        $newDevice->save();
                    }
                }
            }
            //return refreshed devices
            $response_json =   $this->getListOfDevicesJson();
            return Response::json(
                $response_json,
                200
            );
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Cannot store devices",
                'code' => 'E202'
            ),
                404
            );
        }
    }


    private function getListOfDevicesJson($groupID='all'){
        //get all devices from database - TO DO -make better solution than fetching all devices,some count function
        if($groupID == 'all'){
            $devices = Device::all();
        }else{
            $devices = Group::where('id', '=', $groupID)->where('user_id', '=', Auth::id())->first()->devices;
        }

        //return devices from database
        $response_json = new \stdClass();
        $response_json->devices=array();

        foreach($devices as $item){
            $response_json->devices[]=["id"=>$item->id,"name"=>$item->name,"type"=>$item->type];
        }
        //return json with devices
        return $response_json;
    }
}