<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Library\DeviceClass;
use App\Models\Device;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use Mockery\CountValidator\Exception;
use Response;




class DeviceController extends BaseController {


    public function showDeviceDetails($device_id){

        try {
            //$device=Device::find($device_id);
            $device = new DeviceClass($device_id);
        }catch (\Exception $e){
            return Response::json(array(
                'message' => 'Id of device not found',
                'code'    => 'E301'
            ),
                404
            );
        }





        try{
            $device->FetchItems();

            return Response::json(
                $device->MakeJsonResponse(),
                200
            );
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Internal server error",
                'code'    => 'E303'
            ),
                404
            );
        }

    }


    public function MakeAction($device_id){
        try {
            //$device=Device::find($device_id);
            $device = new DeviceClass($device_id);
        }catch (\Exception $e){
            return Response::json(array(
            'message' => 'Id of device not found',
            'code'    => 'E301'
        ),
            404
        );
    }






            $device->FetchItems();
        //echo'tu';
            //make actions
            $data = (object)Input::all();
            //var_dump($data->actions);
            if(isset($data->actions) && is_array($data->actions)){
                foreach($data->actions as $action){
                    $device->MakeAction($action["name"],$action["value"]);
                }
            }

            return Response::json(
                $device->MakeJsonResponse(),
                200
            );
        try{
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Internal server error",
                'code'    => 'E303'
            ),
                404
            );
        }

    }

}