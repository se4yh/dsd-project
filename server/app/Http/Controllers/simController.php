<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;


use App\Library\DeviceClass;
use App\Models\Device;
use App\Models\Group;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Response;
use URL;
require_once app_path().'/Library/Options.php';
//use App\Models\Device;

class SimController extends BaseController
{


    public function showDevices()
    {
        $devices = Device::all();
        $devecesClasses=[];
        foreach($devices as $device){
            $d_class = new DeviceClass($device["id"]);
            $d_class->FetchItems();
            $devecesClasses[]=$d_class;
        }

        return view('simulators')
            ->with("devices",$devecesClasses);





    }


    public function doAction($deviceId)
    {
        if (Request::ajax())
        {
            $device = new DeviceClass($deviceId);
            $device->FetchItems();
            //print_r($device->GetItems());
            foreach($device->GetItems() as $item){
                if($item["type"]=="SWITCH") {//Switch treat separetly (checkboxes not passed vhen not checked :(( )
                    if (array_key_exists($item["name"], $_REQUEST) && $item["value"] != "ON" || !array_key_exists($item["name"], $_REQUEST) && $item["value"] != "OFF") {
                        echo("do action switch");
                        $device->MakeAction($item["name"], (array_key_exists($item["name"], $_REQUEST)) ? "ON" : "OFF");
                    }
                }else if($item["type"]=="CONTACT"){
                    $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
                    $context = stream_context_create(array(
                            'http' => array(
                                'method' => 'PUT',
                                'header'  => 'Content-type: text/plain',
                                'content' =>$_REQUEST[$item["name"]],

                                //'data' => $_REQUEST[$item["name"]]
                            )
                        )
                    );

                    // TO DO - WHEN MOCKUPS/DEVICES ARE DONE MAKE REAL ACTIONS

                    $json_from_OH = json_decode(file_get_contents("http://localhost:8080/rest/items/" . $device->GetName()."__".$device->GetType()."__".$item["name"]."/state", false, $context));
                    var_dump($json_from_OH);
                    var_dump("http://localhost:8080/rest/items/" . $device->GetName()."__".$device->GetType()."__".$item["name"]."/state");
                   //http_put_data("http://localhost:8080/CMD?" . $device->GetName()."__".$device->GetType()."__".$item["name"],$_REQUEST[$item["name"]]);
                }else{
                    if(isset($_REQUEST[$item["name"]]) && $item["value"] != $_REQUEST[$item["name"]]){
                        echo("do action switch".$item["name"]);
                        $device->MakeAction($item["name"] , $_REQUEST[$item["name"]]);
                    }
                }
            }
        }

    }

    public function refreshDevice($deviceId)
    {
        if (Request::ajax())
        {
            $device = new DeviceClass($deviceId);
            $device->FetchItems();
            return json_encode($device->GetItems());
        }
        return;
    }

}