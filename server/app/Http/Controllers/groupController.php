<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.11.2015.
 * Time: 18:31
 */

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\User;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Group;
use Input;
use Auth;
use Response;
use stdClass;

class GroupController extends BaseController {

    public function addNewGroup(){
        //check if group name is provided in req, and that it doesn't exist allready for this user
		if (!Input::has('name') || Group::where('name', '=', Input::get('name'))->where('user_id',Auth::id())->count() > 0) {
            return Response::json(array(
                'message' => "name is not provided or already exists",
                'code' => "E101"
            ),
                404
            );
        };

        try {
            //add new group to database
            $group = new Group;
            $group->name = Input::get('name');
            if(Input::has('description'))$group->info = Input::get('description');
            $group->user_id = Auth::id();
            if(Input::has('type'))$group->type = Input::get('type');
            $group->save();
            return Response::json(array(
                'message' => "group added"
            ),
                200
            );

        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Can't add new group",
                'code' => "E102"
            ),
                404
            );
        }

    }

    public function showGroupsAll(){

        try {
            //fetch all user groups that belongs to user
            $groups = User::find(Auth::id())->groups;

            //define return array
            $returnGroups= array();
            $returnGroups["groups"]= array();
            foreach($groups as $group){
                $temp = new stdClass;
                $temp->id = $group->id;
                $temp->name = $group->name;
                $temp->type = $group->type;
                $temp->description = $group->info;
                $returnGroups["groups"][]=$temp;
            }
            return Response::json(
                $returnGroups,
                200
            );
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "internal server error",
                'code' => "E103"
            ),
                404
            );
        }

    }

    public function groupsAssign(){
        $data = (object)Input::all();
        //is the input data set?
        if(isset($data->group_id) && isset($data->device_id)) {

            //is the input data correct?
            $group=Group::where('id', '=', $data->group_id)->where('user_id', '=', Auth::id())->first();
            if ($group != null  && !is_array($data->device_id) && Device::where('id','=',$data->device_id)->first() != null) {
                //all good
                try{
                    $group->devices()->attach($data->device_id);
                }catch (\Exception $e){
                    //querry exeption
                    return Response::json(array(
                        'message' => "device already in group",
                        'code' => "E104"
                    ),
                        404
                    );
                }
                //return success
                return Response::json(array(
                    'message' => "device added"
                ),
                    200
                );
            }else if($group != null  && is_array($data->device_id)){

                $all_right=true;
                foreach($data->device_id as $device_id){

                    if(Device::where('id','=',$device_id)->first() == null){
                        $all_right=false;
                        break;
                    }


                    try{
                        $group->devices()->attach($device_id);
                    }catch (\Exception $e){
                        //querry exeption
                       /* return Response::json(array(
                            'message' => "device already in group",
                            'code' => "E104"
                        ),
                            404
                        );*/
                    }
                }
                //return success
                return Response::json(array(
                    'message' => "device added"
                ),
                    200
                );
            }
        }

        //semthing not right
        return Response::json(array(
            'message' => "device not added",
            'code' => "E105"
        ),
            404
        );
    }

    public function deleteGroup(){
        try {
            if(!Input::has('ids')) throw new \Exception;
            $group_ids = Input::get('ids');
            foreach ($group_ids as $group_id) {
                Group::destroy($group_id);
            }

            return Response::json(array(
                'message' => "groups deleted"
                ),
                200
            );
        }catch (\Exception $e){
            return Response::json(array(
                'message' => "Cannot delete groups",
                'code' => "E106"
            ),
                404
            );
        }
    }

}