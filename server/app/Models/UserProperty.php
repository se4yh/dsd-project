<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProperty extends Model {

    protected $fillable = array ('user_rule_id','property_id','value');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_properties';

    protected $primaryKey = 'id';

    public function property()
    {
        return $this->belongsTo('App\Models\Property');
    }


}