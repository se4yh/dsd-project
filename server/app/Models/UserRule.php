<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRule extends Model {

    protected $fillable = array ('rule_id','user_id','serialized_data');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_rules';

    protected $primaryKey = 'id';

    public function rule()
    {
        return $this->belongsTo('App\Models\Rule');
    }

    public function device()
    {
        return $this->belongsTo('App\Models\Device');
    }

    public function properties()
    {
        return $this->hasMany('App\Models\UserProperty','user_rule_id');
    }

}