<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model {

    protected $fillable = array ('device_type','name','text');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rules';

    protected $primaryKey = 'id';


    public function properties(){
        return $this->belongsToMany('App\Models\Property');
    }

}