<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model {

    protected $fillable = array ('name','type');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'properties';

    protected $primaryKey = 'id';


}