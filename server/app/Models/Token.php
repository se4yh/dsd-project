<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model {

    protected $fillable = array ('user_id','token','expires');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tokens';

    protected $primaryKey = 'token';


    //*** RELATIONS ***

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}