<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model {

    protected $fillable = array ('name','type','state');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devices';

    protected $primaryKey = 'id';

    public function groups(){
        return $this->belongsToMany('App\Models\Group');
    }

    public function groupsNames()
    {
        $group_names=[];
        $groups=(Device::find($this->getAttributeValue('id'))->groups);
        foreach($groups as $group){
            $group_names[]=$group->name;
        }
        return $group_names;
    }
}