<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

    protected $fillable = array ('name','info','user_id','type');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    protected $primaryKey = 'id';

    /*get devices that belong to a group trough group_devices
     *
     *
     */
    public function devices(){
        return $this->belongsToMany('App\Models\Device');
    }
}