<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.12.2015.
 * Time: 14:28
 */

namespace App\Library;


function getItems($device_type=null){
    //items for suported devices
    $items=[
        "heater" => [//heatmister
            "switch" => "SWITCH_e",
            "room_temperature" => "NUMBER",
            "set_temperature" => "NUMBER_e"
        ],
        "light" => [//simple light switch
            "switch" => "SWITCH_e"
        ],
        "air_conditioner" => [//dakin air conditioner
            "power" => "SWITCH_e",
            "tempin" => "NUMBER",
            "tempout" => "NUMBER",
            "humidin" => "NUMBER",
            "tempset" => "NUMBER_e",
            "mode" => "STRING_e",
            "fan" => "STRING_e"
        ],
        "contact" => [//simple contact gpio item e.g. window contact
            "contact" => "CONTACT"
        ],
        //example
        "device-name" => [
            "item1" => "SWITCH_e", //add _e if it is editble
            "item2" => "NUMBER_e",
            "item3" => "STRING"
        ]

    ];

    if($device_type) {
        //return device items
        if (isset($items[$device_type])) {
            return $items[$device_type];
        } else {
            return null;
        }
    }else{
        //return keys
        return array_keys($items);
    }
}

//define all options for rules
$options=[
    /*
     * "key" => [ "type"=> _optiontype_ , "args" => [_additional arg1_,_additional arg2_]]
     *
     * */

    "o1" => ["type" => "select" , "args" => ["light"]] //option for selecting light type items


];

