<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.12.2015.
 * Time: 13:29
 */

namespace App\Library;


class Item {
    private $type;
    private $name;
    private $value;

    function __construct($name , $type, $value=NULL){
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
    }

    function GetValue($refresh = FALSE){
        if($refresh){
            //get new value from openHAB
            $agent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
            $context = stream_context_create(array(
                    'http' => array(
                        'method' => 'GET',
                        'agent' => $agent,
                        'header' => "Content-Type: application/json\r\n"
                    )
                )
            );
            $json_from_OH = json_decode(file_get_contents("http://localhost:8080/rest/items/" . $this->name . "?type=json", false, $context));
            $this->value=$json_from_OH->state;
        }
        return $this->value;
    }

    function SetValue($value){
        //implementiraj zahtjev na openHAB
        $this->value=$value;
    }



}