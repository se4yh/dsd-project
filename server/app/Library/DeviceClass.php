<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.12.2015.
 * Time: 13:05
 */

namespace App\Library;

use \App\Models\Device;

require_once "Options.php";


 class DeviceClass {

    private $name;
    private $type;
    private $items;
    private $rules;
    private $id;


    /*
     *  $type defined type
     *  $items array of items in openHAB for this device
     *  $rules array of rules aplied for this device
     *
     * */
    function __construct($id){
        //fetch device record
        $deviceData = Device::find($id);
        $this->name = $deviceData->name;
        $this->type = $deviceData->type;
        $this->id   = $id;
        //dd($deviceData->type);
        //dd(getItems($deviceData->type));

        //make items structure
        foreach(getItems($this->type) as $key=>$item){
            $itemTypeData=explode("_",$item);
            $this->items[$key]=["name"=>$key,"type"=>$itemTypeData[0],"value"=>"UNINITIALIZED"];
            if(isset($itemTypeData[1])){
                $this->items[$key]["editable"]="YES";
            }else{
                $this->items[$key]["editable"]="NO";
            }
        }

        $this->rules=[];

        //fetch all the items and put them into the class

    }

    function AddItem($item){
        $this->items[]=$item;
    }

    function AddItems($items){
        foreach($items as $item){
            $this->items[]=$item;
        }
    }

    function GetItems(){
        return $this->items;
    }

    function GetItem($itemName){
        foreach($this->items as $item){
            if($item->name == $itemName) return $item;
        }
    }

    function GetValue($itemName){
        foreach ($this->items as $item) {
            if ($item->name == $itemName) return $item->GetValue();
        }
    }

    function AddRule($rule){
        $this->rules[] = $rule;
    }

    function GetId(){
        return $this->id;
    }

    function GetRules(){
        return $this->rules;
    }

    function GetName(){
        return $this->name;
    }

    function GetType(){
        return $this->type;
    }

    function FetchItems($selectedItems = null){
        if($selectedItems == null){
            foreach($this->items as $key =>$item) {
                //fetch fesh data for the device from OpenHAB
                try {

                    $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
                    $context = stream_context_create(array(
                            'http' => array(
                                'method' => 'GET',
                                'agent' => $agent,
                                'header' => "Content-Type: application/json\r\n"
                            )
                        )
                    );
                    $json_from_OH = json_decode(file_get_contents("http://localhost:8080/rest/items/" . $this->name."__".$this->type."__".$item["name"]. "?type=json", false, $context));
                    $this->items[$key]["value"] = $json_from_OH->state;

                } catch (\Exception $e) {
                    $this->items[$key]["value"]="CANNOT GET VALUE";
                }
            }
        }else{
            foreach($selectedItems as $itemName) {
                //fetch fesh data for the device from OpenHAB
                try {
                    $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
                    $context = stream_context_create(array(
                            'http' => array(
                                'method' => 'GET',
                                'agent' => $agent,
                                'header' => "Content-Type: application/json\r\n"
                            )
                        )
                    );
                    $json_from_OH = json_decode(file_get_contents("http://localhost:8080/rest/items/" . $this->name."__".$this->type."__".$itemName. "?type=json", false, $context));
                    $this->items[$itemName]["value"] = $json_from_OH->state;
                } catch (\Exception $e) {
                    $this->items[$itemName]["value"]="CANNOT GET VALUE";
                }
            }
        }
    }




    /*
     * $string = devicename_xxxxx_deviceType_itemName [living_room_Heater_00001_heater_switchHeating]
     *
     */
    function parseString($string){
        $data=[];
        $fragments=explode('_',$string);
        $lenght=count($fragments);
        $data["type"]=$fragments[$lenght-2];
        $data["item"]=$fragments[$lenght-1];
        //nadji bolje rijesenje od ovog
        unset($fragments[$lenght-1]);
        $data["name"]=implode('_',$fragments);

        return $data;
    }


    function MakeJsonResponse(){
        //make json response
        $response_json = new \stdClass();
        $response_json->device=new  \stdClass();
        $response_json->device->id = $this->id;
        $response_json->device->type = $this->type;
        $response_json->device->name = $this->name;
        $response_json->device->state = null;
        $response_json->device->groups = Device::find($this->id)->groupsNames();
        $response_json->device->items = [];

        foreach($this->items as $item){
            $i = new \stdClass();
            $i->name    = $item["name"];
            $i->value   = $item["value"];
            $i->type    = $item["type"];
            $i->editable = $item["editable"];

            $response_json->device->items[]=$i;
        }

        return $response_json;
    }

    function MakeAction($itemName,$newValue){
        $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        $context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'agent' => $agent,
                    'header' => "Content-Type: application/json\r\n"
                )
            )
        );
        // TO DO - WHEN MOCKUPS/DEVICES ARE DONE MAKE REAL ACTIONS

        $json_from_OH = json_decode(file_get_contents("http://localhost:8080/CMD?" . $this->name."__".$this->type."__".$itemName."=".$newValue, false, $context));

        //refresh item value
        $this->FetchItems([$itemName]);
    }

     function GetItemName($ItemKey){
         return $this->name."__".$this->type."__".$this->items[$ItemKey]["name"];
     }
}