package com.dsd.se4yh.application;

public interface Constants {

    String SHARED_PREFERENCES_NAME = "com.dsd.se4yh.shared_preferences";
    String INTENT_EXTRA_DEVICE_ID = "com.dsd.se4yh.EXTRA.device_id";
    String INTENT_EXTRA_GROUP_ID ="com.dsd.se4yh.EXTRA.group_id";
    String INTENT_EXTRA_GROUP_NAME ="com.dsd.se4yh.EXTRA.group.name";

    /* sharedPreferencesKey  */
    String KEY_USER = "KEY_USER";
    String KEY_TOKEN = "KEY_TOKEN";
    String KEY_AUTOMATIC_LOGIN = "KEY_AUTOMATIC_LOGIN";
    String KEY_URL = "KEY_URL";

    String TYPE_STRING = "STRING";
    String TYPE_NUMBER = "NUMBER";
    String TYPE_SWITCH = "SWITCH";
    String TYPE_TIME = "TIME";
    String TYPE_DATE = "DATE";
    String TYPE_CONTACT = "CONTACT";

    String SWITCH_ON = "ON";
    String SWITCH_OFF = "OFF";

    String UNINITIALIZED = "Uninitialized";
    String EDITABLE_YES = "YES";

    /* Device Type */
    String DEVICE_TYPE_HEATER = "heater";
    String DEVICE_TYPE_COOLER = "air_conditioner";
    String DEVICE_TYPE_LIGHT = "light";
    String DEVICE_TYPE_ENERGY_METER = "meter";
}
