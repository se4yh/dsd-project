package com.dsd.se4yh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.adapter.DeviceDetailsAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.Utils;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.data.DeviceAction;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.DeviceDetailsResponse;
import com.dsd.se4yh.service.configuration.SendActionRequest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class DeviceDetailsActivity extends AppCompatActivity {

    @Inject
    APIServicePrivate apiPrivate;

    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listDetails;
    @Bind(R.id.list_error_message)
    TextView tvError;

    private int deviceId;
    private DeviceDetailsAdapter adapter;

    // region LifeCircle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (App.getUserComponent(this) == null) {
            if (!App.recreateUserComponent(this)) {
                finish();
                return;
            }
        }

        App.getUserComponent(this).injectDeviceDetailsActivity(this);

        setContentView(R.layout.activity_device_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        Intent intent = getIntent();

        if (intent == null) {
            finish();
        } else {
            deviceId = intent.getIntExtra(Constants.INTENT_EXTRA_DEVICE_ID, -1);
            if (deviceId == -1) {
                showError(getString(R.string.no_device_id));
            } else {
                listDetails.setDivider(null);
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getDeviceDetails(deviceId);
                    }
                });
                getDeviceDetails(deviceId);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_device_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_send:
                sendAction();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    // endregion

    // region Network calls
    private void getDeviceDetails(int id) {
        swipeRefreshLayout.setRefreshing(true);
        Call<DeviceDetailsResponse> call = apiPrivate.getDevice(id);
        call.enqueue(new Callback<DeviceDetailsResponse>() {
            @Override
            public void onResponse(Response<DeviceDetailsResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    fillList(response.body().getDevice());
                } else {
                    showError(ErrorUtils.parseDeviceDetailsResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showError(null);
            }
        });
    }

    private void sendAction() {
        Utils.hideKeyboard(this);
        ArrayList<DeviceAction> actions = new ArrayList<>();
        for (DeviceDetailsAdapter.DeviceInput deviceInput : adapter.getDeviceInputList()) {
            actions.add(new DeviceAction(deviceInput.getTitle(), deviceInput.getValue()));
        }
        SendActionRequest sendActionRequest = new SendActionRequest(actions);
        Call<DeviceDetailsResponse> call = apiPrivate.sendAction(deviceId, sendActionRequest);
        swipeRefreshLayout.setRefreshing(true);
        call.enqueue(new Callback<DeviceDetailsResponse>() {
            @Override
            public void onResponse(Response<DeviceDetailsResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    fillList(response.body().getDevice());
                } else {
                    showError(ErrorUtils.parseDeviceDetailsResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showError(null);
            }
        });
    }
    // endregion

    // region View
    private void fillList(Device device) {
        removeError();
        adapter = new DeviceDetailsAdapter(this);
        adapter.addItem(getString(R.string.name), device.getPrettyName());
        adapter.addItem(getString(R.string.type), device.getPrettyType());
        if (device.getGroups() != null && !device.getGroups().isEmpty()) {
            String allGroups = TextUtils.join(", ", device.getGroups());
            adapter.addItem(getString(R.string.assigned_to_groups), allGroups);
        }

        adapter.addItem("", "Items");
        List<Device.DeviceItem> items = device.getItems();
        if (items != null && !items.isEmpty()) {
            for (Device.DeviceItem item : items) {
                adapter.addDeviceInputItem(item.getName(), item.getType(), item.getValue(),
                        item.getEditable().equals(Constants.EDITABLE_YES));
            }
        }

        listDetails.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void showError(String message) {
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.no_internet_connection);
        }
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listDetails.setVisibility(View.GONE);
    }

    private void removeError() {
        tvError.setVisibility(View.GONE);
        listDetails.setVisibility(View.VISIBLE);
    }
    // endregion
}
