package com.dsd.se4yh.service.configuration;

import java.util.List;

public class DeleteRulesRequest {
    private List<Integer> ids;

    public DeleteRulesRequest(List<Integer> ids) {
        this.ids = ids;
    }
}
