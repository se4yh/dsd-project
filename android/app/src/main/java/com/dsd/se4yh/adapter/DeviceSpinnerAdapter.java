package com.dsd.se4yh.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dsd.se4yh.application.data.Device;

import java.util.List;

public class DeviceSpinnerAdapter extends ArrayAdapter<Device> {

    private List<Device> list;

    public DeviceSpinnerAdapter(Context context, List<Device> objects) {
        super(context, android.R.layout.simple_spinner_dropdown_item, objects);
        list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = super.getView(position, convertView, parent);

        TextView textView = (TextView)v.findViewById(android.R.id.text1);
        Device device = list.get(position);
        textView.setText(device.getPrettyName());

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);

        Device device = list.get(position);
        ((TextView)v.findViewById(android.R.id.text1)).setText(device.getPrettyName());

        return v;
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }

    @Override
    public Device getItem(int position) {
        return list.get(position);
    }
}