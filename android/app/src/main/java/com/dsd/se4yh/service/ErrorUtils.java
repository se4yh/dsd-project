package com.dsd.se4yh.service;

import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.service.configuration.DeviceDetailsResponse;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.service.configuration.ListGroupsResponse;
import com.dsd.se4yh.service.configuration.ListRulesResponse;
import com.dsd.se4yh.service.configuration.LoginResponse;
import com.dsd.se4yh.service.configuration.PropertiesResponse;
import com.dsd.se4yh.service.configuration.RuleResponse;
import com.dsd.se4yh.service.configuration.RulesResponse;
import com.squareup.okhttp.ResponseBody;

import java.lang.annotation.Annotation;

import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class ErrorUtils {

    public static String parseBaseResponse(Response<BaseResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseLoginResponse(Response<LoginResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseListDevicesResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseListGroupsResponse(Response<ListGroupsResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseDeviceDetailsResponse(Response<DeviceDetailsResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseListRulesResponse(Response<ListRulesResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parsePropertiesResponse(Response<PropertiesResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseRuleDeviceResponse(Response<RulesResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }

    public static String parseRuleResponse(Response<RuleResponse> response, Retrofit retrofit) {
        Converter<ResponseBody, APIError> converter =
                retrofit.responseConverter(APIError.class, new Annotation[0]);
        APIError error;
        try {
            error = converter.convert(response.errorBody());
            return error.getMessage();
        } catch (Exception e) {
            return "Wrong response from server";
        }
    }
}
