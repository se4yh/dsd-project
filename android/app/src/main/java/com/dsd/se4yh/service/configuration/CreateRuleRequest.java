package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Rule;

public class CreateRuleRequest {

    private Rule rule;

    public CreateRuleRequest(Rule rule) {
        this.rule = rule;
    }
}
