package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Group;

import java.util.ArrayList;

public class ListGroupsResponse extends BaseResponse {

    private ArrayList<Group> groups;

    public ArrayList<Group> getGroups() {
        return groups;
    }
}
