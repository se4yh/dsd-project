package com.dsd.se4yh.service;

import com.dsd.se4yh.service.configuration.AddGroupRequest;
import com.dsd.se4yh.service.configuration.AssignGroupRequest;
import com.dsd.se4yh.service.configuration.AssignGroupResponse;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.service.configuration.CreateRuleRequest;
import com.dsd.se4yh.service.configuration.DeleteGroupsRequest;
import com.dsd.se4yh.service.configuration.DeleteRulesRequest;
import com.dsd.se4yh.service.configuration.DeviceDetailsResponse;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.service.configuration.ListGroupsResponse;
import com.dsd.se4yh.service.configuration.ListRulesResponse;
import com.dsd.se4yh.service.configuration.PropertiesResponse;
import com.dsd.se4yh.service.configuration.RuleResponse;
import com.dsd.se4yh.service.configuration.RulesResponse;
import com.dsd.se4yh.service.configuration.SendActionRequest;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface APIServicePrivate {


    String X_ACCESS_TOKEN = "X-AccessToken";

    /** AUTHENTICATION */
    @GET("/authentication/logout")
    Call<BaseResponse> logout();

    /** DEVICE*/
    @GET("/devices/all")
    Call<ListDevicesResponse> getListOfDevices();

    @GET("/refreshdevices")
    Call<ListDevicesResponse> refreshDevices();

    @GET("/devices/{id}")
    Call<ListDevicesResponse> getDeviceByGroupId(@Path("id") int id);

    @GET("/device/details/{id}")
    Call<DeviceDetailsResponse> getDevice(@Path("id") int id);

    @POST("/device/action/{id}")
    Call<DeviceDetailsResponse> sendAction(@Path("id") int id, @Body SendActionRequest sendActionRequest);

    /** GROUP */
    @GET("/groups/all")
    Call<ListGroupsResponse> getListOfGroups();

    @POST("/group/addnew")
    Call<BaseResponse> addGroup(@Body AddGroupRequest addGroupRequest);

    @POST("/group/delete")
    Call<BaseResponse> deleteGroup(@Body DeleteGroupsRequest deleteGroupsRequest);

    @POST("/groups/assign")
    Call<AssignGroupResponse> assignGroup(@Body AssignGroupRequest assignGroupRequest);

    /** RULE */
    @GET("/rules/all")
    Call<ListRulesResponse> getListOfRules();

    @POST("/rules/addnew")
    Call<BaseResponse> createRule(@Body CreateRuleRequest createRuleRequest);

    @POST("/rules/delete")
    Call<BaseResponse> deleteRules(@Body DeleteRulesRequest deleteRulesRequest);

    @GET("/rules/bydevice/{id}")
    Call<RulesResponse> getRulesForDevice(@Path("id") int id);

    @GET("/rules/properties/{id}")
    Call<PropertiesResponse> getProperties(@Path("id") int id);

    @GET("/rules/getrule/{id}")
    Call<RuleResponse> getRuleById(@Path("id") int id);

}
