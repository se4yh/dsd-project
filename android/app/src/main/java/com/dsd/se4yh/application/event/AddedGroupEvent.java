package com.dsd.se4yh.application.event;

/**
 * Created by mariusschlinke on 09.12.15.
 */
public class AddedGroupEvent {

    private String message;

    public AddedGroupEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
