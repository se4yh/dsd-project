package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Rule;

import java.util.List;

public class RulesResponse {

    private List<Rule> rules;

    public List<Rule> getRules() {
        return rules;
    }
}
