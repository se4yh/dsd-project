package com.dsd.se4yh.application.module;

import com.dsd.se4yh.application.component.UserScope;
import com.dsd.se4yh.application.data.User;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    private User user;
    private String token;
    private String url;

    public UserModule(User user, String token, String url) {
        this.user = user;
        this.token = token;
        this.url = url;
    }

    @Provides
    @UserScope
    User provideUser(){
        return user;
    }

    @Provides
    @UserScope
    @Named("token")
    String provideToken() {
        return token;
    }

    @Provides
    @UserScope
    @Named("url")
    String provideUrl() {
        return url;
    }
}
