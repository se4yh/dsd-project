package com.dsd.se4yh.service;

/**
 * Created by mariusschlinke on 22.11.15.
 */

import com.dsd.se4yh.service.configuration.LoginRequest;
import com.dsd.se4yh.service.configuration.LoginResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

public interface APIServicePublic {

    @POST("/authentication/login")
    Call<LoginResponse> getAccessToken(@Body LoginRequest loginRequest);
}

