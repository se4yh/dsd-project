package com.dsd.se4yh.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Paint;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.Utils;
import com.dsd.se4yh.application.data.Property;

import java.util.Calendar;
import java.util.List;

import timber.log.Timber;

public class PropertyListAdapter extends ArrayAdapter<Property> implements Constants {

    private static final int VIEW_NUMBER = 0;
    private static final int VIEW_TIME = 1;
    private static final int VIEW_DATE = 2;
    private static final int VIEW_SWITCH = 3;
    private static final int VIEW_COUNT = 4;

    private List<Property> list;
    private Calendar calendar;
    private boolean isTempInvalid;

    public PropertyListAdapter(Context context, List<Property> list) {
        super(context, 0);
        this.list = list;
        calendar = Calendar.getInstance();
        isTempInvalid = false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case VIEW_NUMBER:
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.list_item_number, parent, false);
                break;
            case VIEW_TIME:
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.list_item_dialog, parent, false);
                break;
            case VIEW_DATE:
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.list_item_dialog, parent, false);
                break;
            case VIEW_SWITCH:
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.list_item_switch, parent, false);
                break;
        }

        if (convertView == null) {
            return null;
        }

        final Property property = list.get(position);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
        tvTitle.setText(property.getName());

        String value = property.getValue();
        boolean setValue = false;
        if (!TextUtils.isEmpty(value)) {
            setValue = true;
        }

        switch (viewType) {
            case VIEW_NUMBER:
                EditText etNumber = (EditText) convertView.findViewById(R.id.edt_list_item_number);
                if (setValue) {
                    etNumber.setText(value);
                } else {
                    isTempInvalid = true;
                }
                etNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        property.setValue(s.toString());
                        try {
                            int value = Integer.valueOf(s.toString());
                            if (value == 0) {
                                isTempInvalid = true;
                            } else {
                                isTempInvalid = false;
                            }
                        } catch (NumberFormatException e) {
                            isTempInvalid = true;
                        }
                    }
                });
                break;

            case VIEW_TIME:
                final TextView tvTime = (TextView) convertView.findViewById(R.id.tv_list_item_dialog);
                tvTime.setPaintFlags(tvTime.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                if (setValue) {
                    String[] values = value.split(" ");
                    try {
                        int minute = Integer.valueOf(values[0]);
                        int hour = Integer.valueOf(values[1]);
                        tvTime.setText(Utils.getTimeString(hour, minute));
                        isTempInvalid = false;
                    } catch (NumberFormatException e) {
                        tvTime.setText(getContext().getString(R.string.select_time));
                        isTempInvalid = true;
                    }
                } else {
                    tvTime.setText(getContext().getString(R.string.select_time));
                    isTempInvalid = true;
                }
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog time = new TimePickerDialog(getContext(),
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                        Timber.d("time on: hour = " + hourOfDay + "; minute = " + minute);
                                        property.setValue("" + minute + " " + hourOfDay);
                                        tvTime.setText(Utils.getTimeString(hourOfDay, minute));
                                        isTempInvalid = false;
                                    }
                                }, 0, 0, false);
                        time.show();
                    }
                });
                break;

            case VIEW_DATE:
                final TextView tvDate = (TextView) convertView.findViewById(R.id.tv_list_item_dialog);
                tvDate.setPaintFlags(tvDate.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                if (setValue) {
                    tvDate.setText(value);
                    isTempInvalid = false;
                } else {
                    tvDate.setText(getContext().getString(R.string.select_date));
                    isTempInvalid = true;
                }
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Dialog date = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                monthOfYear++; // month starts from 0: 0-11
                                String date = "" + dayOfMonth + "." + monthOfYear + "." + year;
                                Timber.d(date);
                                tvDate.setText(date);
                                isTempInvalid = false;
                            }
                        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                        date.show();
                    }
                });
                break;

            case VIEW_SWITCH:
                Switch sw = (Switch) convertView.findViewById(R.id.sw_list_item_switch);
                if (setValue) {
                    if (value.equals(Constants.SWITCH_ON)) {
                        sw.setChecked(true);
                    } else {
                        sw.setChecked(false);
                    }
                }
                sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        property.setValue(String.valueOf(isChecked));
                    }
                });
                break;
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getType()) {
            case TYPE_NUMBER:
                return VIEW_NUMBER;
            case TYPE_TIME:
                return VIEW_TIME;
            case TYPE_DATE:
                return VIEW_DATE;
            case TYPE_SWITCH:
                return VIEW_SWITCH;
        }
        return VIEW_NUMBER;
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_COUNT;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Property getItem(int position) {
        return list.get(position);
    }

    public List<Property> getList() {
        return list;
    }

    public boolean isTempInvalid() {
        return isTempInvalid;
    }
}
