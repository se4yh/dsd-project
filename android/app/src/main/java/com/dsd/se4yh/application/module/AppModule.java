package com.dsd.se4yh.application.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @Singleton
    App provideApp() {
        return app;
    }

    @Provides @Singleton
    SharedPreferences provideSharedPreferences() {
        return app.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Provides @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new Bus(ThreadEnforcer.ANY);
    }
}
