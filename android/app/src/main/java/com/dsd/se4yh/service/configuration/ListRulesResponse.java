package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.RuleList;

import java.util.List;

public class ListRulesResponse {

    private List<RuleList> rules;

    public List<RuleList> getRules() {
        return rules;
    }
}
