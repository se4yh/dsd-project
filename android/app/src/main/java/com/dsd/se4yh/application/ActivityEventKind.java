package com.dsd.se4yh.application;

/**
 * Created by mariusschlinke on 05.12.15.
 */
public enum ActivityEventKind {
    CREATED, DESTROYED
}