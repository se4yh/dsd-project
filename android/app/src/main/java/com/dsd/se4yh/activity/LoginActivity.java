package com.dsd.se4yh.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.User;
import com.dsd.se4yh.service.APIServicePublic;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.APIConstants;
import com.dsd.se4yh.service.configuration.LoginRequest;
import com.dsd.se4yh.service.configuration.LoginResponse;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;


public class LoginActivity extends AppCompatActivity {

    /* View Bindings */
    @Bind(R.id.btn_login_login) Button loginBtn;
    @Bind(R.id.tv_login_error_message) TextView errorMessageView;
    @Bind(R.id.tv_login_resetpassword) TextView resetPassword;
    @Bind(R.id.edt_login_name) EditText loginEdt;
    @Bind(R.id.edt_login_password) EditText passwordEdt;
    @Bind(R.id.cb_login_automatic) CheckBox automaticLoginCb;
    @Bind(R.id.et_server) EditText etServer;

    /*  Inject App Component elements */
    @Inject
    SharedPreferences sharedPreferences;    // use sharedPrefs for storing user info and token
    @Inject
    Gson gson;

    private APIServicePublic apiService = null;
    private boolean loginValid = false;
    private Subscription subscription;
    private ProgressDialog progressDialog;


    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        App.getAppComponent(this).injectLoginActivity(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loadUser();
        formatResetPassword();
        etServer.setText(APIConstants.BASE_URL);
    }

    private void configSubscription() {
            subscription = Observable
                    .empty()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe(launchMainActivity());
    }

    private Subscriber<Object> launchMainActivity() {
        return new Subscriber<Object>() {
            @Override
            public void onCompleted() {
                if (!isUnsubscribed() && loginValid) {
                    closeProgressDialog();
                    startMainActivity();
                }
            }

            @Override
            public void onError(Throwable e) { }

            @Override
            public void onNext(Object o) { }
        };
    }

    private void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);

        this.finish();
    }

    //endregion

    //region Persistence
    private void loadUser() {
        String userJson = sharedPreferences.getString(Constants.KEY_USER, "");
        String token = sharedPreferences.getString(Constants.KEY_TOKEN, "");
        String automaticLogin = sharedPreferences.getString(Constants.KEY_AUTOMATIC_LOGIN, "");
        String url = sharedPreferences.getString(Constants.KEY_URL, "");

        if (!userJson.equals("") && !token.equals("") && !url.equals("") && !automaticLogin.equals("")) {
            User user = gson.fromJson(userJson, User.class);
            boolean isAutomaticLogin = Boolean.valueOf(automaticLogin);
            if (isAutomaticLogin) {
                // create User Component only if proceeding to Main Activity
                createUserComp(user, token, url);
                startMainActivity();
            } else {
                // User Component will be create after successful login
                loginEdt.setText(user.getEmail());
            }
        }
    }
    private void createUserComp(User user, String token, String url) {
        App.createUserComponent(this, user, token, url);
    }

    private void saveUser(User user, String token, String automaticLogin, String url) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String userToJson = gson.toJson(user);
        Timber.d("userJson = " + userToJson);
        editor.putString(Constants.KEY_USER, userToJson);
        editor.putString(Constants.KEY_TOKEN, token);
        editor.putString(Constants.KEY_AUTOMATIC_LOGIN, automaticLogin);
        editor.putString(Constants.KEY_URL, url);
        editor.apply();
    }
    //endregion

    //region Event Handling

    @OnClick(R.id.btn_login_login)
    public void login() {
        if (checkUserInputIsNotEmpty() && checkUrl()) {
            initService(etServer.getText().toString());
            sendLoginRequest();
            showProgressDialog();
        }
    }

    @OnClick(R.id.tv_login_resetpassword)
    public void resetPassword() {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }
    //endregion

    //region Network
    private void sendLoginRequest() {
        Timber.d("username = " + loginEdt.getText().toString());
        Timber.d("password = " + passwordEdt.getText().toString());
        Call<LoginResponse> call = apiService.getAccessToken(
                new LoginRequest(loginEdt.getText().toString(), passwordEdt.getText().toString()));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                // response.isSuccess() is true if the response code is 2xx
                LoginResponse loginResponse = response.body();
                if (response.isSuccess()) {
                    if (loginResponse != null) {
                        configSubscription();
                        successfulLogin(loginResponse);
                    } else {
                        showErrorMessage(getString(R.string.wrong_response_from_server));
                    }
                } else {
                    closeProgressDialog();
                    int statusCode = response.code();
                    Timber.w("login error code = " + statusCode);
                    showErrorMessage(ErrorUtils.parseLoginResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                // handle execution failures like no internet connectivity
                Timber.e("login onFailure");
                handleLoginCallFailure();
                closeProgressDialog();
                if (subscription != null && !subscription.isUnsubscribed()) {
                    Timber.i("IsSubscribed");
                    subscription.unsubscribe();
                }
            }
        });
    }

    private void successfulLogin(LoginResponse loginResponse) {
        errorMessageView.setVisibility(View.GONE);
        User user = loginResponse.getUser();
        boolean automaticLogin = automaticLoginCb.isChecked();
        String token = loginResponse.getToken();
        String url = etServer.getText().toString();
        createUserComp(user, token, url);
        saveUser(user, token, String.valueOf(automaticLogin), url);
        loginValid = true;

    }

    private void initService(String url) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient httpClient = new OkHttpClient();
        // add logging as last interceptor
        httpClient.interceptors().add(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

         apiService = retrofit.create(APIServicePublic.class);
    }
    //endregion

    //region Validation
    private boolean checkUserInputIsNotEmpty() {
        if (loginEdt.getText().toString().isEmpty()) {
            loginEdt.setError(getResources().getString(R.string.login_error_empty_login));
            return false;
        }
        if (passwordEdt.getText().toString().isEmpty()) {
            passwordEdt.setError(getResources().getString(R.string.login_error_empty_password));
            return false;
        }
        return true;
    }

    private boolean checkUrl() {
        return true;
    }
    //endregion

    //region View Manipulation
    private void formatResetPassword() {
        resetPassword.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    private void handleLoginCallFailure() {
        errorMessageView.setText(getString(R.string.no_internet_connection));
        errorMessageView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage(String message) {
        errorMessageView.setVisibility(View.VISIBLE);
        errorMessageView.setText(message);
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.login_progress_dialog));
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
    //endregion
}
