package com.dsd.se4yh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.adapter.DeviceListCheckableAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.event.AssignedDeviceEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.AssignGroupRequest;
import com.dsd.se4yh.service.configuration.AssignGroupResponse;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class AssignDeviceActivity extends AppCompatActivity {

    private int groupId;
    private String groupName;
    private DeviceListCheckableAdapter adapter;
    private Toolbar toolbar;
    private MenuItem assignItem;

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    Bus eventBus;

    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listViewDevices;
    @Bind(R.id.tv_assign_device_error_message)
    TextView tvError;

    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getUserComponent(this).injectAssignDeviceActivity(this);
        setContentView(R.layout.activity_assign_device);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //TODO: getForwared Groupname and set it as header
        ButterKnife.bind(this);
        fetchIntent();
        eventBus.register(this);
    }

    private void fetchIntent() {
        Intent intent = getIntent();

        if (intent == null) {
            finish();
        } else {
            groupId = intent.getIntExtra(Constants.INTENT_EXTRA_GROUP_ID, -1);
            groupName = intent.getStringExtra(Constants.INTENT_EXTRA_GROUP_NAME);
            if (groupId == -1) {
               showError(getString(R.string.assign_device_no_group_id));
            }   else {
                loadDevices();
            }
            if (groupName != "") {
                getSupportActionBar().setTitle(groupName);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        listViewDevices.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Timber.d("onLongClick");
                changeUIForSelection(true);
                assignItem.setVisible(true);
                return false;
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        assignItem = menu.findItem(R.id.icon_confirm).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_assign_device, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.icon_confirm:
                assignDevices();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }

    //endregion

    private void loadDevices() {
        Call<ListDevicesResponse> call = apiPrivate.getListOfDevices();
        call.enqueue(new Callback<ListDevicesResponse>() {
            @Override
            public void onResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    ListDevicesResponse devicesResponse = response.body();
                    if (devicesResponse != null) {
                        if (devicesResponse.getDevices().isEmpty()) {
                            showError(getString(R.string.no_devices));
                        } else {
                            showDevices(devicesResponse.getDevices());
                        }
                    } else {
                        showError(getString(R.string.wrong_response_from_server));
                    }
                } else {
                    Timber.w("error code = " + response.code());
                    showError(ErrorUtils.parseListDevicesResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showError(getString(R.string.no_internet_connection));
            }
        });
    }

    private void assignDevices() {
        List<Integer> devices = adapter.getCheckItemIds();
        if (devices.isEmpty()) {
            changeUIForSelection(false);
            assignItem.setVisible(false);
            return;
        }
        Call<AssignGroupResponse> call = apiPrivate.assignGroup(new AssignGroupRequest(devices, groupId));
        call.enqueue(new Callback<AssignGroupResponse>() {
            @Override
            public void onResponse(Response<AssignGroupResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    handleSuccess();
                } else {
                    int statusCode = response.code();
                    showError(String.valueOf(statusCode));
                }
                assignItem.setVisible(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.i("AddGroup onFailure");
                assignItem.setVisible(false);
            }
        });
    }

    private void handleSuccess() {
        eventBus.post(new AssignedDeviceEvent("device/s assigned"));
        this.finish();
    }


    //region View Manipulation

    private void showDevices(ArrayList<Device> devices) {
        if (devices.size() > 0) {
            adapter = new DeviceListCheckableAdapter(this, devices);
            listViewDevices.setAdapter(adapter);
        }
    }

    private void showError(String message) {
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listViewDevices.setVisibility(View.GONE);
    }

    private void changeUIForSelection(boolean checkable) {
        adapter.setCheckable(checkable);
        adapter.notifyDataSetChanged();
    }
    //endregion
}
