package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.User;

public class LoginResponse extends BaseResponse {

    private String token;
    private User user;

    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }
}
