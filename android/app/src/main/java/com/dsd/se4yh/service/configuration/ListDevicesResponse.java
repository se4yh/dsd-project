package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Device;

import java.util.ArrayList;

public class ListDevicesResponse extends BaseResponse {

    ArrayList<Device> devices;

    public ArrayList<Device> getDevices() {
        return devices;
    }
}
