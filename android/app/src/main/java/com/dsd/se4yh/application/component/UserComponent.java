package com.dsd.se4yh.application.component;

import com.dsd.se4yh.activity.AddGroupActivity;
import com.dsd.se4yh.activity.AssignDeviceActivity;
import com.dsd.se4yh.activity.CreateRuleActivity;
import com.dsd.se4yh.activity.DeviceDetailsActivity;
import com.dsd.se4yh.activity.GroupDetailsActivity;
import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.module.APIPrivateModule;
import com.dsd.se4yh.application.module.UserModule;
import com.dsd.se4yh.fragment.DevicesFragment;
import com.dsd.se4yh.fragment.GroupFragment;
import com.dsd.se4yh.fragment.RuleFragment;

import dagger.Component;

@UserScope
@Component(modules = {UserModule.class, APIPrivateModule.class},
        dependencies = {AppComponent.class})
public interface UserComponent {
    void injectMainActivity(MainActivity activity);
    void injectDevicesFragment(DevicesFragment fragment);
    void injectGroupFragment(GroupFragment fragment);
    void injectRuleFragment(RuleFragment fragment);
    void injectAddGroupActivity(AddGroupActivity activity);
    void injectDeviceDetailsActivity(DeviceDetailsActivity activity);
    void injectGroupDetailsActivity(GroupDetailsActivity activity);
    void injectAssignDeviceActivity(AssignDeviceActivity activity);
    void injectCreteRuleActivity(CreateRuleActivity activity);
}
