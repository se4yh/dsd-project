package com.dsd.se4yh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.adapter.DeviceListAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.event.AssignedDeviceEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class GroupDetailsActivity extends AppCompatActivity {

    private int groupId;
    private String groupName;
    private DeviceListAdapter adapter;


    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    Bus eventBus;

    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listDevices;
    @Bind(R.id.tv_group_details_error_message)
    TextView tvError;


    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getUserComponent(this).injectGroupDetailsActivity(this);

        setContentView(R.layout.activity_group_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAssignDeviceActivity();
            }
        });

        ButterKnife.bind(this);
        eventBus.register(this);
        loadGroupDetails();
        setupList();
    }

    private void setupList() {
        listDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startDeviceDetailsActivity(adapter.getItem(position).getId());
            }
        });
    }

    private void startAssignDeviceActivity() {
        Intent intent = new Intent(GroupDetailsActivity.this, AssignDeviceActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_ID, groupId);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_NAME, groupName);
        startActivity(intent);
    }

    private void startDeviceDetailsActivity(int deviceId) {
        Intent intent = new Intent(GroupDetailsActivity.this, DeviceDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID,deviceId);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }

    //endregion

    private void loadGroupDetails() {
        Intent intent = getIntent();

        if (intent == null) {
            finish();
        } else {
            groupId = intent.getIntExtra(Constants.INTENT_EXTRA_GROUP_ID, -1);
            groupName = intent.getStringExtra(Constants.INTENT_EXTRA_GROUP_NAME);
            if (groupId == -1) {
                showError(getString(R.string.group_details_no_group_id));
            } else {
                listDevices.setDivider(null);
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getGroupDetails(groupId);
                    }
                });
                getGroupDetails(groupId);

            }
            // set header as the groupName
            if (groupName != "") {
                getSupportActionBar().setTitle(groupName);
            }
        }
    }

    //region Network calls
    private void getGroupDetails(int id) {
        swipeRefreshLayout.setRefreshing(true);
        Call<ListDevicesResponse> call = apiPrivate.getDeviceByGroupId(id);
        call.enqueue(new Callback<ListDevicesResponse>() {
            @Override
            public void onResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    ListDevicesResponse devicesResponse = response.body();
                    if (devicesResponse != null) {
                        if (devicesResponse.getDevices().isEmpty()) {
                            showError(getString(R.string.group_details_no_devices_info));
                        } else {
                            showDevices(devicesResponse.getDevices());
                        }
                    } else {
                        showError(getString(R.string.wrong_response_from_server));
                    }
                } else {
                    showError(getString(R.string.wrong_response_from_server));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showError(null);
            }
        });

    }
    //endregion

    //region Event Handling
    @Subscribe
    public void assignedDeviceEvent(AssignedDeviceEvent event) {
        Timber.i("AssignedDeviceEvent received");
        getGroupDetails(groupId);
    }
    //endregion

    // region View Manipulation
    private void showDevices(ArrayList<Device> devices) {
        adapter = new DeviceListAdapter(this, devices);
        listDevices.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
        listDevices.setAdapter(adapter);
    }

    private void showError(String message) {
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listDevices.setVisibility(View.GONE);
    }
    //endregion

}
