package com.dsd.se4yh.service.configuration;

public class BaseResponse {

    private String message;
    private String code;

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
