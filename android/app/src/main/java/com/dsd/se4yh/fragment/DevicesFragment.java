package com.dsd.se4yh.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.activity.DeviceDetailsActivity;
import com.dsd.se4yh.adapter.DeviceListAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.data.User;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class DevicesFragment extends Fragment {

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    User user;

    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listViewDevices;
    @Bind(R.id.list_error_message)
    TextView tvError;

    private DeviceListAdapter adapter;

    public DevicesFragment() {
        // Required empty public constructor
    }

    // region Lifecycle
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_devices, container, false);
        ButterKnife.bind(this, view);

        App.getUserComponent(getActivity()).injectDevicesFragment(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshListOfDevices();
            }
        });
        listViewDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startDeviceDetailsActivity(adapter.getItem(position).getId());
            }
        });
        getListOfDevices();
        return view;
    }
    //endregion

    private void startDeviceDetailsActivity(int id) {
        Intent intent = new Intent(getActivity(), DeviceDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, id);
        startActivity(intent);
    }

    // region Network
    private void refreshListOfDevices() {
        Timber.d("refreshListOfDevices");
        Call<ListDevicesResponse> call = apiPrivate.refreshDevices();
        call.enqueue(new Callback<ListDevicesResponse>() {
            @Override
            public void onResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
                parseDevices(response, retrofit);
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showErrorScreen(getString(R.string.no_internet_connection));
            }
        });
    }

    private void getListOfDevices(){
        Timber.d("getListOfDevices");
        Call<ListDevicesResponse> call = apiPrivate.getListOfDevices();
        call.enqueue(new Callback<ListDevicesResponse>() {
            @Override
            public void onResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
                parseDevices(response, retrofit);
            }

            @Override
            public void onFailure(Throwable t) {
                if (isAdded()) {
                    swipeRefreshLayout.setRefreshing(false);
                    showErrorScreen(getString(R.string.no_internet_connection));
                }
            }
        });
    }
    // endregion

    // region View
    private void parseDevices(Response<ListDevicesResponse> response, Retrofit retrofit) {
        swipeRefreshLayout.setRefreshing(false);
        if (response.isSuccess()) {
            ListDevicesResponse devicesResponse = response.body();
            if (devicesResponse != null) {
                if (devicesResponse.getDevices().isEmpty()) {
                    showErrorScreen(getString(R.string.no_devices));
                } else {
                    showDevices(devicesResponse.getDevices());
                }
            } else {
                showErrorScreen(getString(R.string.wrong_response_from_server));
            }
        } else {
            Timber.w("error code = " + response.code());
            showErrorScreen(ErrorUtils.parseListDevicesResponse(response, retrofit));
        }
    }

    private void showDevices(ArrayList<Device> devices) {
        if (getActivity() != null) {
            adapter = new DeviceListAdapter(getActivity(), devices);
            listViewDevices.setAdapter(adapter);
        }
    }

    private void showErrorScreen(String message) {
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.no_internet_connection);
        }
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listViewDevices.setVisibility(View.GONE);
    }
    //endregion
}
