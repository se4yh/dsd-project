package com.dsd.se4yh.activity;

/**
 * Created by mariusschlinke on 22.11.15.
 */

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.fragment.DevicesFragment;
import com.dsd.se4yh.fragment.GroupFragment;
import com.dsd.se4yh.fragment.RuleFragment;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.configuration.BaseResponse;

import javax.inject.Inject;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    SharedPreferences sharedPreferences;

    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (App.getUserComponent(this) == null) {
            if (!App.recreateUserComponent(this)) {
                startLoginActivity();
            }
        }

        App.getUserComponent(this).injectMainActivity(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        /** per default the {@link GroupFragment}  is selected */
        navigationView.getMenu().performIdentifierAction(R.id.nav_group, 0);
    }

    private void startLoginActivity() {
        Intent login = new Intent(this, LoginActivity.class);
        startActivity(login);
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    //endregion


    //region Event Handling
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle navigation view item clicks here.
        selectDrawerItem(menuItem);
        return true;
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;

        Class fragmentClass = null;
        switch(menuItem.getItemId()) {
            case R.id.nav_group:
                fragmentClass = GroupFragment.class;
                break;
            case R.id.nav_rules:
                fragmentClass = RuleFragment.class;
                break;
            case R.id.nav_devices:
                fragmentClass = DevicesFragment.class;
                break;
            case R.id.nav_logout:
                logoutUser();
                return;
            default:
                fragmentClass = GroupFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commitAllowingStateLoss();

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
    //endregion

    //region Network
    private void logoutUser() {
        Call<BaseResponse> call = apiPrivate.logout();
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {
                Timber.d("logout onResponse");
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("logout onFailure");
            }
        });
        clearUser();
    }
    //endregion

    private void clearUser() {
        App.clearUserComponent(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        startLoginActivity();
    }
}
