package com.dsd.se4yh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.data.Device;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by mariusschlinke on 02.01.16.
 */
public class DeviceListCheckableAdapter extends ArrayAdapter<Device> {

    private Context context;
    private List<DeviceItem> devices;
    private boolean checkable;

    public DeviceListCheckableAdapter(Context context, List<Device> objects) {
        super(context, 0, objects);
        this.context = context;
        this.devices = new ArrayList<>();
        for (Device item : objects) {
            devices.add(new DeviceItem(item));
        }
        this.checkable = false;
    }

    public void setCheckable(boolean checkable) {
        this.checkable = checkable;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_device, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }


        final DeviceItem item = devices.get(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvName.setText(item.device.getPrettyName());

        Bitmap bitmap = loadDynamicImage(item.device.getType());
        if (bitmap != null) {
            holder.ivIcon.setImageBitmap(bitmap);
        } else {
            holder.ivIcon.setImageResource(R.drawable.ic_device_undefined);
        }

        if (checkable) {
            holder.cbCheckable.setVisibility(View.VISIBLE);
            holder.cbCheckable.setChecked(item.isChecked);
            holder.cbCheckable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    item.isChecked = isChecked;
                }
            });
        } else {
            holder.cbCheckable.setVisibility(View.GONE);
        }

        //ViewHolder viewHolder = new ViewHolder(convertView, devices.get(position));
        return convertView;
    }

    /**
     * method which is loading a image for the given type
     * Note: the icon at the drawable folder must follow this schema: ic_device_type
     * e.g.
     *  ic_device_light
     *  ic_device_heater
     *  ic_device_...
     * @param type
     * @return
     */
    private Bitmap loadDynamicImage(String type) {
        String fileBase = "ic_device_"; //  this is image file name
        String fileName = fileBase +  type;
        String PACKAGE_NAME = context.getPackageName();
        int imgId = context.getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + fileName, null, null);
        return BitmapFactory.decodeResource(context.getResources(), imgId);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    public List<Integer> getCheckItemIds() {
        ArrayList<Integer> checked = new ArrayList<>();
        for (DeviceItem item : devices) {
            if (item.isChecked) {
                checked.add(item.device.getId());
            }
        }
        return checked;
    }

    protected class ViewHolder {
        @Bind(R.id.iv_device_icon)
        ImageView ivIcon;
        @Bind(R.id.tv_item_title)
        TextView tvName;
        @Bind(R.id.cb_checkable)
        CheckBox cbCheckable;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public class DeviceItem {
        boolean isChecked;
        Device device;

        public DeviceItem(Device group) {
            this.isChecked = false;
            this.device = group;
        }
    }
}
