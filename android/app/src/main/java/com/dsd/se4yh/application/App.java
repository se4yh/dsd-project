package com.dsd.se4yh.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;

import com.crashlytics.android.Crashlytics;
import com.dsd.se4yh.application.component.AppComponent;
import com.dsd.se4yh.application.component.DaggerAppComponent;
import com.dsd.se4yh.application.component.DaggerUserComponent;
import com.dsd.se4yh.application.component.UserComponent;
import com.dsd.se4yh.application.data.User;
import com.dsd.se4yh.application.module.APIPrivateModule;
import com.dsd.se4yh.application.module.AppModule;
import com.dsd.se4yh.application.module.UserModule;

import io.fabric.sdk.android.Fabric;
import java.util.concurrent.ArrayBlockingQueue;

import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;

public class App extends Application {

    private AppModule appModule;
    private AppComponent appComponent;
    private UserComponent userComponent;
    private static Observable<ActivityEvent> _activityEventStream;

    public static Observable<ActivityEvent> activityEventStream() {
        return _activityEventStream;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Timber.plant(new Timber.DebugTree());
        createAppComponent();
        ActivityEventProducer activityEventProducer = new ActivityEventProducer();
        _activityEventStream = Observable.create(activityEventProducer);
        registerActivityLifecycleCallbacks(activityEventProducer);
    }

    private void createAppComponent() {
        appModule = new AppModule(this);
        appComponent = DaggerAppComponent.builder()
                .appModule(appModule).build();
    }

    private void creteUserComponent(User user, String token, String url) {
        userComponent = DaggerUserComponent.builder()
                .userModule(new UserModule(user, token, url))
                .aPIPrivateModule(new APIPrivateModule())
                .appComponent(appComponent)
                .build();
    }

    public static AppComponent getAppComponent(Context context) {
        return ((App) context.getApplicationContext()).appComponent;
    }

    public static void createUserComponent(Context context, User user, String token, String url) {
        ((App) context.getApplicationContext()).creteUserComponent(user,token, url);
    }

    public static UserComponent getUserComponent(Context context) {
        return ((App) context.getApplicationContext()).userComponent;
    }

    public static void clearUserComponent(Context context) {
        ((App) context.getApplicationContext()).userComponent = null;
    }

    public static boolean recreateUserComponent(Context context) {
        SharedPreferences sharedPreferences = getAppComponent(context).sharedPreferences();
        String userJson = sharedPreferences.getString(Constants.KEY_USER, "");
        String token = sharedPreferences.getString(Constants.KEY_TOKEN, "");
        String url = sharedPreferences.getString(Constants.KEY_URL, "");
        if (!userJson.equals("") && !token.equals("") && !url.equals("")) {
            User user = getAppComponent(context).gson().fromJson(userJson, User.class);
            if (user != null) {
                createUserComponent(context, user, token, url);
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    public void setUserComponent(UserComponent component) {
        userComponent = component;
    }


    /* We need this class for Testing with Espresso and Retrofit */
    private static class ActivityEventProducer implements ActivityLifecycleCallbacks, Observable.OnSubscribe<ActivityEvent> {
        private ArrayBlockingQueue<ActivityEvent> activityEvents = new ArrayBlockingQueue<>(256, false);
        private boolean anyOneSubscribed;


        @Override
        public void onActivityDestroyed(Activity activity) {
            if(!anyOneSubscribed) {
                return;
            }
            ActivityEvent activityEvent = new ActivityEvent();
            activityEvent.setActivityClass(activity.getClass());
            activityEvent.setEventKind(ActivityEventKind.DESTROYED);
            activityEvents.add(activityEvent);
        }

        @Override
        public void call(Subscriber<? super ActivityEvent> subscriber) {
            anyOneSubscribed = true;
            try {
                while(!subscriber.isUnsubscribed()) {
                    ActivityEvent activityEvent = activityEvents.take();
                    subscriber.onNext(activityEvent);
                }
            } catch(Exception e) {
                subscriber.onError(e);
            } finally {
                anyOneSubscribed = false;
                activityEvents.clear();
            }
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if(!anyOneSubscribed) {
                return;
            }
            ActivityEvent activityEvent = new ActivityEvent();
            activityEvent.setActivityClass(activity.getClass());
            activityEvent.setEventKind(ActivityEventKind.CREATED);
            activityEvents.add(activityEvent);
        }

        @Override
        public void onActivityStarted(Activity activity) {
            if(!anyOneSubscribed) {
                return;
            }
        }

        @Override
        public void onActivityResumed(Activity activity) {
            if(!anyOneSubscribed) {
                return;
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            if(!anyOneSubscribed) {
                return;
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {
            if(!anyOneSubscribed) {
                return;
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            if(!anyOneSubscribed) {
                return;
            }
        }
    }
}
