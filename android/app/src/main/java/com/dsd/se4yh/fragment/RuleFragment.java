package com.dsd.se4yh.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.activity.CreateRuleActivity;
import com.dsd.se4yh.adapter.RuleListAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.data.RuleList;
import com.dsd.se4yh.application.event.CreateRuleEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.service.configuration.DeleteRulesRequest;
import com.dsd.se4yh.service.configuration.ListRulesResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

/**
 * Created by mariusschlinke on 22.11.15.
 */
public class RuleFragment extends Fragment {

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    Bus eventBus;

    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listRules;
    @Bind(R.id.list_error_message)
    TextView tvError;

    private RuleListAdapter adapter;
    private MenuItem menuItemDelete;

    public RuleFragment() {
        // Required empty public constructor
    }

    // region Lifecycle
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rule, container, false);
        ButterKnife.bind(this, view);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOfRules();
            }
        });
        listRules.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Timber.d("rules onItemLongClick");
                changeUiForDelete(true);
                return false;
            }
        });
        listRules.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startCreateRuleActivity(adapter.getItem(position).getId());
            }
        });

        App.getUserComponent(getActivity()).injectRuleFragment(this);

        eventBus.register(this);
        setHasOptionsMenu(true);

        getListOfRules();

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menuItemDelete.setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menuItemDelete = menu.findItem(R.id.icon_delete);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.icon_delete) {
            deleteRules();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        eventBus.unregister(this);
    }

    private void startCreateRuleActivity(int id) {
        Intent intent = new Intent(getActivity(), CreateRuleActivity.class);
        intent.putExtra(CreateRuleActivity.EXTRA_RULE_ID, id);
        startActivity(intent);
    }
    // endregion


    // region Subscribe
    @Subscribe
    public void createRuleEvent(CreateRuleEvent event) {
        Timber.i("create rule event handled");
        getListOfRules();
    }
    // endregion


    // region OnClick Events
    @OnClick(R.id.fab_add_rule)
    public void onClickAddRule() {
        startCreateRuleActivity(0);
    }
    // endregion


    // region Network
    private void getListOfRules() {
        swipeRefreshLayout.setRefreshing(true);
        Call<ListRulesResponse> call = apiPrivate.getListOfRules();
        call.enqueue(new Callback<ListRulesResponse>() {
            @Override
            public void onResponse(Response<ListRulesResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    refreshList(response.body().getRules());
                } else {
                    showErrorScreen(ErrorUtils.parseListRulesResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showErrorScreen(null);
            }
        });
    }

    private void deleteRules() {
        List<Integer> ids = adapter.getSelectedRules();
        if (ids.isEmpty()) {
            menuItemDelete.setVisible(false);
            changeUiForDelete(false);
            return;
        }

        Call<BaseResponse> call = apiPrivate.deleteRules(new DeleteRulesRequest(ids));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    getListOfRules();
                } else {
                    showErrorScreen(ErrorUtils.parseBaseResponse(response, retrofit));
                }
                menuItemDelete.setVisible(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.w("delete rules onFailure");
                showErrorScreen(null);
                menuItemDelete.setVisible(false);
            }
        });
    }
    // endregion

    // region View
    private void refreshList(List<RuleList> rules) {
        if (rules.isEmpty()) {
            showErrorScreen(getString(R.string.no_rules));
            return;
        }
        adapter = new RuleListAdapter(getActivity(), rules);
        listRules.setAdapter(adapter);
        listRules.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
    }

    private void showErrorScreen(String message) {
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.no_internet_connection);
        }
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listRules.setVisibility(View.GONE);
    }

    private void changeUiForDelete(boolean delete) {
        adapter.setDelete(delete);
        adapter.notifyDataSetChanged();
        if (menuItemDelete != null) {
            menuItemDelete.setVisible(delete);
        }
    }
    // endregion
}
