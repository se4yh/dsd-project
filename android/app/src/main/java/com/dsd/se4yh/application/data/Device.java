package com.dsd.se4yh.application.data;

import java.util.List;

public class Device {

    private int id;
    private String type;
    private String name;
    private List<DeviceItem> items;
    private List<String> groups;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getPrettyType() {
        return type.replace("_", " ");
    }

    public String getName() {
        return name;
    }

    public String getPrettyName() {
        return name.replace("_", " ");
    }

    public List<DeviceItem> getItems() {
        return items;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class DeviceItem {
        private String name;
        private String type;
        private String value;
        private String editable;

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }

        public String getEditable() {
            return editable;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
