package com.dsd.se4yh.application;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Utils {

    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //TODO: refactor this
    public static String formatGroupType(String groupType) {
        String result ="default";
        if (groupType.equals("default (optional)")) {
            result = "default";
        }
        if (groupType.equals("Bathroom")) {
            result ="bathroom";
        }
        if (groupType.equals("Bedroom")) {
            result ="bedroom";
        }
        if (groupType.equals("Children Room")) {
            result ="childrenroom";
        }
        if (groupType.equals("Corridor")) {
            result ="corridor";
        }
        if (groupType.equals("Kitchen")) {
            result ="kitchen";
        }
        if (groupType.equals("Living Room")) {
            result ="livingroom";
        }
        if (groupType.equals("Office")) {
            result ="office";
        }
        if (groupType.equals("Terrace")) {
            result ="terrace";
        }
        if (groupType.equals("Toilet")) {
            result ="toilet";
        }
        if (groupType.equals("Office")) {
            result ="office";
        }
        if (groupType.equals("Washingroom")) {
            result ="washingroom";
        }

        return result;
    }

    public static String getTimeString(int hour, int min) {
        NumberFormat formatter = new DecimalFormat("00");
        return formatter.format(hour) + ":" + formatter.format(min);
    }

}
