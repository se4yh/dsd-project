package com.dsd.se4yh.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.activity.AddGroupActivity;
import com.dsd.se4yh.activity.GroupDetailsActivity;
import com.dsd.se4yh.adapter.GroupListAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.Group;
import com.dsd.se4yh.application.event.AddedGroupEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.service.configuration.DeleteGroupsRequest;
import com.dsd.se4yh.service.configuration.ListGroupsResponse;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;


/**
 * Created by mariusschlinke on 22.11.15.
 */

public class GroupFragment extends Fragment {

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    Bus eventBus;


    @Bind(R.id.list_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.list)
    ListView listViewGroups;
    @Bind(R.id.list_error_message)
    TextView tvError;

    private GroupListAdapter adapter;
    private MenuItem menuItemDelete;

    public GroupFragment() {
        // Required empty public constructor
    }

    //region Lifecycle
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);
        ButterKnife.bind(this, view);

        App.getUserComponent(getActivity()).injectGroupFragment(this);
        eventBus.register(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListOfGroups();
            }
        });
        listViewGroups.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Timber.d("onLongClick");
                changeUIForDelete(true);
                return false;
            }
        });

        listViewGroups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startGroupDetailsActivity(adapter.getItem(position).getId(), adapter.getItem(position).getName());
            }
        });
        getListOfGroups();
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        eventBus.unregister(this);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menuItemDelete.setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menuItemDelete = menu.findItem(R.id.icon_delete);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.icon_delete) {
            deleteGroups();
            return true;
        }
        return false;
    }

    private void startGroupDetailsActivity(int id, String groupName) {
        Intent intent = new Intent(getActivity(), GroupDetailsActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_ID, id);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_NAME, groupName);
        startActivity(intent);
    }

    private void startAddGroupActivity() {
        Intent intent = new Intent(getActivity(), AddGroupActivity.class);
        startActivity(intent);
    }
    //endregion


    //region Event Handling
    @Subscribe
    public void addedGroupEvent(AddedGroupEvent event) {
        Timber.i("AddedGroupEvent received");
        getListOfGroups();
    }
    //endregion


    //region Network
    private void getListOfGroups() {
        swipeRefreshLayout.setRefreshing(true);
        tvError.setVisibility(View.GONE);
        Call<ListGroupsResponse> call = apiPrivate.getListOfGroups();
        call.enqueue(new Callback<ListGroupsResponse>() {
            @Override
            public void onResponse(Response<ListGroupsResponse> response, Retrofit retrofit) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccess()) {
                    ListGroupsResponse groupsResponse = response.body();
                    if (groupsResponse != null && !groupsResponse.getGroups().isEmpty()) {
                        showDevices(groupsResponse.getGroups());
                    } else {
                        showError(getString(R.string.no_groups));
                    }
                } else {
                    int statusCode = response.code();
                    Timber.w("getgroups error code = " + statusCode);
                    showError(ErrorUtils.parseListGroupsResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                showError(null);
            }
        });
    }

    private void deleteGroups() {
        List<Integer> groups = adapter.getCheckItemIds();
        if (groups.isEmpty()) {
            menuItemDelete.setVisible(false);
            changeUIForDelete(false);
            return;
        }
        Call<BaseResponse> call = apiPrivate.deleteGroup(new DeleteGroupsRequest(groups));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    getListOfGroups();
                } else {
                    showError(ErrorUtils.parseBaseResponse(response, retrofit));
                }
                menuItemDelete.setVisible(false);
            }

            @Override
            public void onFailure(Throwable t) {
                showError(null);
                menuItemDelete.setVisible(false);
            }
        });
    }
    //endregion

    //region View handling
    private void showDevices(ArrayList<Group> groups) {
        if (getActivity() != null) {
            adapter = new GroupListAdapter(getActivity(), groups);
        }
        listViewGroups.setVisibility(View.VISIBLE);
        listViewGroups.setAdapter(adapter);
    }

    private void showError(String message) {
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.no_internet_connection);
        }
        tvError.setText(message);
        tvError.setVisibility(View.VISIBLE);
        listViewGroups.setVisibility(View.GONE);
    }

    private void changeUIForDelete(boolean delete) {
        adapter.setDelete(delete);
        adapter.notifyDataSetChanged();
        if (menuItemDelete != null) {
            menuItemDelete.setVisible(delete);
        }
    }
    //endregion

    // region OnClick Events
    @OnClick(R.id.fab_add_group)
    public void onClickAdd() {
        startAddGroupActivity();
    }
    // endregion
}
