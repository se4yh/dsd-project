package com.dsd.se4yh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.data.Device;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DeviceListAdapter extends ArrayAdapter<Device> {

    private Context context;
    private List<Device> devices;

    public DeviceListAdapter(Context context, List<Device> objects) {
        super(context, 0, objects);
        this.context = context;
        this.devices = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_device, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }


        final Device item = devices.get(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvName.setText(item.getPrettyName());

        Bitmap bitmap = loadDynamicImage(item.getType());
        if (bitmap != null) {
            holder.ivIcon.setImageBitmap(bitmap);
        } else {
            holder.ivIcon.setImageResource(R.drawable.ic_device_undefined);
        }

        //ViewHolder viewHolder = new ViewHolder(convertView, devices.get(position));
        return convertView;
    }

    /**
     * method which is loading a image for the given type
     * Note: the icon at the drawable folder must follow this schema: ic_device_type
     * e.g.
     *  ic_device_light
     *  ic_device_heater
     *  ic_device_...
     * @param type
     * @return
     */
    private Bitmap loadDynamicImage(String type) {
        String fileBase = "ic_device_"; //  this is image file name
        String fileName = fileBase +  type;
        String PACKAGE_NAME = context.getPackageName();
        int imgId = context.getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + fileName, null, null);
        return BitmapFactory.decodeResource(context.getResources(),imgId);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Device getItem(int position) {
        return devices.get(position);
    }

    protected class ViewHolder {
        @Bind(R.id.iv_device_icon)
        ImageView ivIcon;
        @Bind(R.id.tv_item_title)
        TextView tvName;

        public ViewHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
