package com.dsd.se4yh.service.configuration;

import java.util.List;

/**
 * Created by mariusschlinke on 29.12.15.
 */
public class AssignGroupRequest {

    private List<Integer> device_id;
    private int group_id;

    public AssignGroupRequest(List<Integer> ids, int groupId) {
        this.device_id = ids;
        this.group_id = groupId;
    }
}
