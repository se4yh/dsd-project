package com.dsd.se4yh.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dsd.se4yh.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPasswordActivity extends AppCompatActivity {

    @Bind(R.id.btn_reset_password_send) Button resetPasswordBtn;
    @Bind(R.id.edt_reset_password_email) EditText resetPwdEdt;
    @Bind(R.id.tv_reset_password_error_message) TextView errorMessageView;

    //region Lifcycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
    }
    //endregion


    //region Event Handling
    @OnClick(R.id.btn_reset_password_send)
    public void resetPassword() {
        if (checkUserInputIsNotEmpty()) {
            if (checkEmail()) {
                errorMessageView.setVisibility(View.GONE);
                //TODO:sendResetRequest
            } else {
                errorMessageView.setVisibility(View.VISIBLE);
            }
        }
    }
    //endregion

    //region Validation
    private boolean checkEmail() {
        //TODO: check if user exists
        return false;
    }

    private boolean checkUserInputIsNotEmpty() {
        if (resetPwdEdt.getText().toString().isEmpty()) {
            resetPwdEdt.setError(getResources().getString(R.string.resetpassword_empty_login));
            return false;
        }
        return true;
    }
    //endregion
}
