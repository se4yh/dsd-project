package com.dsd.se4yh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dsd.se4yh.R;
import com.dsd.se4yh.adapter.DeviceSpinnerAdapter;
import com.dsd.se4yh.adapter.PropertyListAdapter;
import com.dsd.se4yh.adapter.RuleSpinnerAdapter;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Utils;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.data.Property;
import com.dsd.se4yh.application.data.Rule;
import com.dsd.se4yh.application.event.CreateRuleEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.service.configuration.CreateRuleRequest;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.service.configuration.PropertiesResponse;
import com.dsd.se4yh.service.configuration.RuleResponse;
import com.dsd.se4yh.service.configuration.RulesResponse;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class CreateRuleActivity extends AppCompatActivity {

    public static final String EXTRA_RULE_ID = "CreateRuleActivity.extra_rule_id";

    @Bind(R.id.rl_error_view)
    View viewError;
    @Bind(R.id.tv_error_view)
    TextView tvError;
    @Bind(R.id.spinner_select_device)
    Spinner spinnerDevice;
    @Bind(R.id.spinner_select_rule)
    Spinner spinnerRule;
    @Bind(R.id.list_properties)
    ListView listProperties;
    @Bind(R.id.et_rule_name)
    EditText etRuleName;

    @Inject
    APIServicePrivate apiService;
    @Inject
    Bus eventBus;

    private DeviceSpinnerAdapter deviceSpinnerAdapter;
    private RuleSpinnerAdapter ruleSpinnerAdapter;
    private PropertyListAdapter propertyListAdapter;
    private Rule updateRule;

    // region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (App.getUserComponent(this) == null) {
            if (!App.recreateUserComponent(this)) {
                finish();
                return;
            }
        }

        App.getUserComponent(this).injectCreteRuleActivity(this);

        setContentView(R.layout.activity_create_rule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);


        Intent intent = getIntent();
        if (intent != null) {
            int ruleId = intent.getIntExtra(EXTRA_RULE_ID, 0);
            if (ruleId != 0) {
                getRuleById(ruleId);
                return;
            }
        }

        spinnerDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Timber.d("selected item: " + deviceSpinnerAdapter.getItem(position).getPrettyName());
                if (position != deviceSpinnerAdapter.getCount()) {
                    getRulesByDevice(deviceSpinnerAdapter.getItem(position).getId());
                } else {
                    hideRuleAndProperties();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Timber.d("spinner device: nothing selected");
            }
        });

        spinnerRule.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != ruleSpinnerAdapter.getCount()) {
                    getAllProperties(ruleSpinnerAdapter.getItem(position).getId());
                } else {
                    hideProperties();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Timber.d("spinner rule: nothing selected");
            }
        });

        getAllDevices();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_create_rule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_create_rule:
                Timber.d("Action create rule");
                createRuleCall();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // endregion

    // region Network
    private void getAllDevices() {
        Call<ListDevicesResponse> call = apiService.getListOfDevices();
        call.enqueue(new Callback<ListDevicesResponse>() {
            @Override
            public void onResponse(Response<ListDevicesResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    fillDeviceSpinner(response.body().getDevices());
                } else {
                    Timber.w("getDevices: failed");
                    showErrorScreen(ErrorUtils.parseListDevicesResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("getDevices: onFailure");
                showErrorScreen(null);
            }
        });
    }

    private void getRulesByDevice(int id) {
        Call<RulesResponse> call = apiService.getRulesForDevice(id);
        call.enqueue(new Callback<RulesResponse>() {
            @Override
            public void onResponse(Response<RulesResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    fillRuleSpinner(response.body().getRules());
                } else {
                    Timber.w("getRulesByDevice: failed");
                    showErrorScreen(ErrorUtils.parseRuleDeviceResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("getRulesByDevice: onFailure");
                showErrorScreen(null);
            }
        });
    }

    private void getAllProperties(int id) {
        Call<PropertiesResponse> call = apiService.getProperties(id);
        call.enqueue(new Callback<PropertiesResponse>() {
            @Override
            public void onResponse(Response<PropertiesResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    fillPropertyList(response.body().getProperties());
                } else {
                    Timber.w("getAllProperties: failed");
                    showErrorScreen(ErrorUtils.parsePropertiesResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("getAllProperties: onFailure");
                showErrorScreen(null);
            }
        });
    }

    private void createRuleCall() {
        Utils.hideKeyboard(this);

        String ruleName = etRuleName.getText().toString().trim();
        if (TextUtils.isEmpty(ruleName)) {
            etRuleName.setText("");
            etRuleName.setError(getString(R.string.rule_no_name));
            return;
        }

        Rule rule;
        if (updateRule == null) {
            int positionDevice = spinnerDevice.getSelectedItemPosition();
            int deviceId = deviceSpinnerAdapter.getItem(positionDevice).getId();
            int positionRule = spinnerRule.getSelectedItemPosition();
            int ruleId = ruleSpinnerAdapter.getItem(positionRule).getId();

            rule = new Rule();
            rule.setId(ruleId);
            rule.setDeviceId(deviceId);
        } else {
            rule = updateRule;
        }

        if (propertyListAdapter.isTempInvalid()) {
            Toast.makeText(this, getString(R.string.value_must_set), Toast.LENGTH_SHORT).show();
            return;
        }

        rule.setName(ruleName);
        rule.setProperties(propertyListAdapter.getList());
        Timber.d(rule.toString());

        CreateRuleRequest createRuleRequest = new CreateRuleRequest(rule);
        Call<BaseResponse> call = apiService.createRule(createRuleRequest);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    // finish activity on successfully created rule
                    eventBus.post(new CreateRuleEvent());
                    finish();
                } else {
                    Timber.w("getDevices: failed");
                    showErrorScreen(ErrorUtils.parseBaseResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("createRuleRequest: onFailure");
                showErrorScreen(null);
            }
        });
    }

    private void getRuleById(int ruleId) {
        Call<RuleResponse> call = apiService.getRuleById(ruleId);
        call.enqueue(new Callback<RuleResponse>() {
            @Override
            public void onResponse(Response<RuleResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    showEditRule(response.body().getRule());
                } else {
                    Timber.w("getRuleById: failed");
                    showErrorScreen(ErrorUtils.parseRuleResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.e("getRuleById: onFailure");
                showErrorScreen(null);
            }
        });
    }
    // endregion

    // region View
    private void showErrorScreen(String message) {
        spinnerDevice.setVisibility(View.GONE);
        hideRuleAndProperties();
        viewError.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(message)) {
            message = getString(R.string.addgroup_error_message);
        }
        tvError.setText(message);
    }

    private void fillDeviceSpinner(ArrayList<Device> devices) {
        if (devices.size() == 0) {
            showErrorScreen(getString(R.string.rule_no_device));
            return;
        }
        Device hint = new Device();
        hint.setName(getString(R.string.choose_device));
        devices.add(hint);
        deviceSpinnerAdapter = new DeviceSpinnerAdapter(this, devices);
        spinnerDevice.setAdapter(deviceSpinnerAdapter);
        spinnerDevice.setSelection(deviceSpinnerAdapter.getCount());
    }

    private void fillRuleSpinner(List<Rule> rules) {
        Rule hint = new Rule();
        hint.setName(getString(R.string.choose_rule));
        rules.add(hint);
        ruleSpinnerAdapter = new RuleSpinnerAdapter(this, rules);
        spinnerRule.setAdapter(ruleSpinnerAdapter);
        spinnerRule.setVisibility(View.VISIBLE);
        spinnerRule.setSelection(ruleSpinnerAdapter.getCount());
    }

    private void fillPropertyList(List<Property> properties) {
        propertyListAdapter = new PropertyListAdapter(this, properties);
        listProperties.setAdapter(propertyListAdapter);
        listProperties.setVisibility(View.VISIBLE);
        etRuleName.setVisibility(View.VISIBLE);
    }

    private void showEditRule(Rule rule) {
        spinnerDevice.setVisibility(View.GONE);
        spinnerRule.setVisibility(View.GONE);
        listProperties.setVisibility(View.VISIBLE);
        etRuleName.setVisibility(View.VISIBLE);

        etRuleName.setText(rule.getName());
        propertyListAdapter = new PropertyListAdapter(this, rule.getProperties());
        listProperties.setAdapter(propertyListAdapter);

        updateRule = rule;
    }

    private void hideRuleAndProperties() {
        spinnerRule.setVisibility(View.GONE);
        hideProperties();
    }

    private void hideProperties() {
        listProperties.setVisibility(View.GONE);
        etRuleName.setVisibility(View.GONE);
    }
    // endregion
}
