package com.dsd.se4yh.application.data;

public class Group {

    private int id;
    private String name;
    private String type;
    private String description;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() { return type; }

    public String getDescription() {
        return description;
    }
}
