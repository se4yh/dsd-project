package com.dsd.se4yh.application.data;

public class Property {

    private int id;
    private int userPropertyId;
    private String name;
    private String type;
    private String value;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Property{" +
                "id=" + id +
                ", userPropertyId=" + userPropertyId +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
