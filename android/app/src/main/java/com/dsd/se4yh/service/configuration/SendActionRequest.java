package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.DeviceAction;

import java.util.List;

public class SendActionRequest {

    private List<DeviceAction> actions;

    public SendActionRequest(List<DeviceAction> actions) {
        this.actions = actions;
    }
}
