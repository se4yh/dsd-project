package com.dsd.se4yh.application.data;

public class RuleList {

    private int id;
    private String ruleName;
    private String deviceName;
    private boolean isChecked;

    public int getId() {
        return id;
    }

    public String getRuleName() {
        return ruleName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDevicePrettyName() {
        return deviceName.replace("_", " ");
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
