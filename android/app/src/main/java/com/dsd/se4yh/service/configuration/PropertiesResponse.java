package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Property;

import java.util.List;

public class PropertiesResponse {

    private List<Property> properties;

    public List<Property> getProperties() {
        return properties;
    }
}
