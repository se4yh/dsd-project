package com.dsd.se4yh.application.component;

import android.content.SharedPreferences;

import com.dsd.se4yh.activity.AddGroupActivity;
import com.dsd.se4yh.activity.LoginActivity;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.module.AppModule;
import com.google.gson.Gson;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component( modules = {AppModule.class})
public interface AppComponent {
    /*  inject component  */
    void injectLoginActivity(LoginActivity activity);
    //void injectAddGroupActivity(AddGroupActivity activity);


    /*  provide outside component */
    App provideApp();
    SharedPreferences sharedPreferences();
    Gson gson();
    Bus bus();
}
