package com.dsd.se4yh.application.event;

/**
 * Created by mariusschlinke on 03.01.16.
 */
public class AssignedDeviceEvent {

    private String message;

    public AssignedDeviceEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
