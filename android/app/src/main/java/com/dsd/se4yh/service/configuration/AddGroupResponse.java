package com.dsd.se4yh.service.configuration;

/**
 * Created by mariusschlinke on 18.12.15.
 */
public class AddGroupResponse extends BaseResponse {

    private String name;
    private String description;

    public AddGroupResponse(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
