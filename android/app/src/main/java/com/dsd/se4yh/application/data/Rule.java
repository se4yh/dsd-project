package com.dsd.se4yh.application.data;

import java.util.List;

public class Rule {

    private int id;
    private int deviceId;
    private int userRuleId;
    private String name;
    private List<Property> properties;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public int getUserRuleId() {
        return userRuleId;
    }

    public void setUserRuleId(int userRuleId) {
        this.userRuleId = userRuleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Rule{" +
                "id=" + id +
                ", deviceId=" + deviceId +
                ", userRuleId=" + userRuleId +
                ", name='" + name + '\'' +
                ", properties=" + properties +
                '}';
    }
}
