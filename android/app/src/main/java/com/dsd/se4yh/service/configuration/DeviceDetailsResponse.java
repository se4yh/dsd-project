package com.dsd.se4yh.service.configuration;

import com.dsd.se4yh.application.data.Device;

public class DeviceDetailsResponse extends BaseResponse {

    private Device device;

    public Device getDevice() {
        return device;
    }
}
