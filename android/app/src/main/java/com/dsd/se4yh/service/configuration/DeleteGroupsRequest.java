package com.dsd.se4yh.service.configuration;

import java.util.List;

public class DeleteGroupsRequest {
    private List<Integer> ids;

    public DeleteGroupsRequest(List<Integer> ids) {
        this.ids = ids;
    }
}
