package com.dsd.se4yh.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dsd.se4yh.application.data.Rule;

import java.util.List;

public class RuleSpinnerAdapter extends ArrayAdapter<Rule> {

    private List<Rule> list;

    public RuleSpinnerAdapter(Context context, List<Rule> list) {
        super(context, android.R.layout.simple_spinner_dropdown_item, list);
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = super.getView(position, convertView, parent);

        TextView textView = (TextView)v.findViewById(android.R.id.text1);

        Rule item = getItem(position);
        textView.setText(item.getName());

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = super.getView(position, convertView, parent);

        Rule item = getItem(position);
        ((TextView)v.findViewById(android.R.id.text1)).setText(item.getName());

        return v;
    }

    @Override
    public Rule getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public int getCount() {
        // don't display last item. It is used as hint.
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }
}
