package com.dsd.se4yh.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.data.Group;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GroupListAdapter extends ArrayAdapter<Group> {

    private Context context;
    private List<GroupItem> groups;
    private boolean delete;

    public GroupListAdapter(Context context, List<Group> objects) {
        super(context, 0, objects);
        this.context = context;
        this.groups = new ArrayList<>();
        for (Group item : objects) {
            groups.add(new GroupItem(item));
        }
        this.delete = false;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_group, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        final GroupItem item = groups.get(position);

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvTitle.setText(item.group.getName());
        holder.tvSubtitle.setText(item.group.getDescription());

        Bitmap bitmap = loadDynamicImage(item.group.getType());
        if (bitmap != null) {
            holder.ivIcon.setImageBitmap(bitmap);
        } else {
            holder.ivIcon.setImageResource(R.drawable.ic_device_undefined);
        }

        if (delete) {
            holder.cbDelete.setVisibility(View.VISIBLE);
            holder.cbDelete.setChecked(item.isChecked);
            holder.cbDelete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    item.isChecked = isChecked;
                }
            });
        } else {
            holder.cbDelete.setVisibility(View.GONE);
        }

        return convertView;
    }

    /**
     * method which is loading a image for the given type
     * Note: the icon at the drawable folder must follow this schema: ic_device_type
     * e.g.
     *  ic_group_kitchen
     *  ic_group_childrenroom
     *  ic_group_default
     * @param type
     * @return
     */
    private Bitmap loadDynamicImage(String type) {
        String fileBase = "ic_group_"; //  this is image file name
        String fileName = fileBase +  type;
        String PACKAGE_NAME = context.getPackageName();
        int imgId = context.getResources().getIdentifier(PACKAGE_NAME + ":drawable/" + fileName, null, null);
        return BitmapFactory.decodeResource(context.getResources(), imgId);
    }

    @Override
    public int getCount() {
        return groups.size();
    }

    public List<Integer> getCheckItemIds() {
        ArrayList<Integer> checked = new ArrayList<>();
        for (GroupItem item : groups) {
            if (item.isChecked) {
                checked.add(item.group.getId());
            }
        }
        return checked;
    }

    protected class ViewHolder {
        @Bind(R.id.iv_group_icon)
        ImageView ivIcon;
        @Bind(R.id.tv_item_title)
        TextView tvTitle;
        @Bind(R.id.tv_item_subtitle)
        TextView tvSubtitle;
        @Bind(R.id.cb_delete)
        CheckBox cbDelete;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public class GroupItem {
        boolean isChecked;
        Group group;

        public GroupItem(Group group) {
            this.isChecked = false;
            this.group = group;
        }
    }
}
