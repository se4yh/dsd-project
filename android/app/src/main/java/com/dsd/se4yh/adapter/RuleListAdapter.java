package com.dsd.se4yh.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.data.RuleList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RuleListAdapter extends ArrayAdapter<RuleList> {

    private List<RuleList> list;
    private boolean delete;

    public RuleListAdapter(Context context, List<RuleList> resource) {
        super(context, 0);
        this.list = resource;
        this.delete = false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_rule, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        final RuleList rule = list.get(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();

        holder.title.setText(rule.getRuleName());
        holder.subtitle.setText(rule.getDevicePrettyName());

        if (delete) {
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    rule.setChecked(isChecked);
                }
            });
        } else {
            holder.delete.setVisibility(View.GONE);
            holder.delete.setChecked(false);
            rule.setChecked(false);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public RuleList getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public List<Integer> getSelectedRules() {
        ArrayList<Integer> selectedRules = new ArrayList<>();
        for (RuleList ruleList : list) {
            if (ruleList.isChecked()) {
                selectedRules.add(ruleList.getId());
            }
        }
        return selectedRules;
    }

    protected class ViewHolder {
        @Bind(R.id.tv_rule_title)
        TextView title;
        @Bind(R.id.tv_rule_subtitle)
        TextView subtitle;
        @Bind(R.id.cb_rule_delete)
        CheckBox delete;

        public ViewHolder(View view) {
            ButterKnife.bind(this,view);
        }
    }
}
