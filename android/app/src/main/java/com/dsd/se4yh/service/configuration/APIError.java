package com.dsd.se4yh.service.configuration;

public class APIError {

    private String message;
    private String code;

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}