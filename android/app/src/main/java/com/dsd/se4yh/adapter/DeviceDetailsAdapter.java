package com.dsd.se4yh.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.Constants;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class DeviceDetailsAdapter extends BaseAdapter implements Constants {

    public static final int TYPE_TITLE = 1;
    public static final int TYPE_INPUT_SWITCH = 2;
    public static final int TYPE_INPUT_TEXT = 3;
    public static final int TYPE_INPUT_NUMBER = 4;
    public static final int TYPE_INPUT_CONTACT = 5;
    public static final int NUMBER_OF_TYPES = 6;

    private Context context;
    private List<DeviceDescription> list;

    public DeviceDetailsAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    public void addItem(String title, String description) {
        list.add(new DeviceDescription(title, description));
    }

    public void addDeviceInputItem(String title, String type, String value, boolean editable) {
        list.add(new DeviceInput(title, type, value, editable));
    }

    public List<DeviceInput> getDeviceInputList() {
        ArrayList<DeviceInput> deviceInputArrayList = new ArrayList<>();
        for (DeviceDescription device : list) {
            if (device instanceof DeviceInput) {
                deviceInputArrayList.add((DeviceInput) device);
            }
        }
        return deviceInputArrayList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public DeviceDescription getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        switch (getItemViewType(position)) {
            case TYPE_TITLE:
                DeviceDescription item = list.get(position);
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_device_details, parent, false);
                TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_device_details_title);
                TextView tvDescription = (TextView) convertView.findViewById(R.id.tv_device_details_description);
                tvTitle.setText(item.getPrettyTitle());
                tvDescription.setText(item.description);
                break;

            case TYPE_INPUT_SWITCH:
                DeviceInput inputSwitch = (DeviceInput) list.get(position);
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_switch, parent, false);
                TextView tvSwitchTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
                Switch swSwitch = (Switch) convertView.findViewById(R.id.sw_list_item_switch);
                tvSwitchTitle.setText(inputSwitch.getPrettyTitle());
                if (SWITCH_ON.equals(inputSwitch.value)) {
                    swSwitch.setChecked(true);
                } else {
                    swSwitch.setChecked(false);
                }
                swSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        changeSwitchValue(position, isChecked);
                    }
                });
                break;

            case TYPE_INPUT_NUMBER:
                DeviceInput inputNumber = (DeviceInput) list.get(position);
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_number, parent, false);
                TextView tvNumberTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
                EditText edtNumber = (EditText) convertView.findViewById(R.id.edt_list_item_number);
                tvNumberTitle.setText(inputNumber.getPrettyTitle());
                if (!TextUtils.isEmpty(inputNumber.value)
                        && !inputNumber.value.equals(Constants.UNINITIALIZED)) {
                    edtNumber.setText(inputNumber.value);
                }

                if (inputNumber.editable) {
                    edtNumber.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            updateValue(position, s.toString());
                        }
                    });
                } else {
                    edtNumber.setKeyListener(null);
                    edtNumber.setBackground(null);
                }
                break;

            case TYPE_INPUT_TEXT:
                DeviceInput inputText = (DeviceInput) list.get(position);
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_text, parent, false);
                TextView tvTextTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
                EditText edtText = (EditText) convertView.findViewById(R.id.edt_list_item_text);
                tvTextTitle.setText(inputText.getPrettyTitle());
                if (!TextUtils.isEmpty(inputText.value)
                        && !inputText.value.equals(Constants.UNINITIALIZED)) {
                    edtText.setText(inputText.value);
                }
                edtText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        updateValue(position, s.toString());
                    }
                });
                break;

            case TYPE_INPUT_CONTACT:
                DeviceInput inputContact = (DeviceInput) list.get(position);
                convertView = LayoutInflater.from(context)
                        .inflate(R.layout.list_item_dialog, parent, false);
                TextView tvContactTitle = (TextView) convertView.findViewById(R.id.tv_list_item_title);
                tvContactTitle.setText(inputContact.getPrettyTitle());
                TextView tvStatus = (TextView) convertView.findViewById(R.id.tv_list_item_dialog);
                tvStatus.setText(inputContact.value);
                break;
            default:
                Timber.e("wrong view type");
                return null;
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return NUMBER_OF_TYPES;
    }

    @Override
    public int getItemViewType(int position) {
        DeviceDescription item = list.get(position);
        if (item instanceof DeviceInput) {
            DeviceInput input = (DeviceInput) item;
            if (TYPE_SWITCH.equals(input.type)) {
                return TYPE_INPUT_SWITCH;
            } else if (TYPE_NUMBER.equals(input.type)) {
                return TYPE_INPUT_NUMBER;
            } else if (TYPE_CONTACT.equals(input.type)) {
                return TYPE_INPUT_CONTACT;
            } else {
                return TYPE_INPUT_TEXT;
            }
        } else {
            return TYPE_TITLE;
        }
    }

    private void changeSwitchValue(int position, boolean isChecked) {
        DeviceInput deviceInput = (DeviceInput) list.get(position);
        if (isChecked) {
            deviceInput.value = SWITCH_ON;
        } else {
            deviceInput.value = SWITCH_OFF;
        }
        Timber.d("switch = " + isChecked + "; real = " + deviceInput.value);
    }

    private void updateValue(int position, String value) {
        DeviceInput deviceInput = (DeviceInput) list.get(position);
        deviceInput.value = value;
        Timber.d("update value for " + deviceInput.title + "; new = " + deviceInput.value);
    }

    public class DeviceDescription {
        String title;
        String description;

        public DeviceDescription(String title, String description) {
            this.title = title;
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public String getPrettyTitle() {
            return title.replace("_", " ");
        }

        public String getDescription() {
            return description;
        }
    }

    public class DeviceInput extends DeviceDescription {
        String type;
        String value;
        boolean editable;

        public DeviceInput(String title, String type, String value, boolean editable) {
            super(title, null);
            this.type = type;
            this.value = value;
            this.editable = editable;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }
    }
}
