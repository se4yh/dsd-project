package com.dsd.se4yh.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dsd.se4yh.R;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Utils;
import com.dsd.se4yh.application.event.AddedGroupEvent;
import com.dsd.se4yh.service.APIServicePrivate;
import com.dsd.se4yh.service.ErrorUtils;
import com.dsd.se4yh.service.configuration.AddGroupRequest;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import timber.log.Timber;

public class AddGroupActivity extends AppCompatActivity {

    @Bind(R.id.edt_addgroup_groupname) EditText addGroupNameEdt;
    @Bind(R.id.edt_addgroup_groupdescription) EditText addGroupDescriptionEdt;
    @Bind(R.id.btn_addgroup_create) Button addGroupBtn;
    @Bind(R.id.tv_addgroup_error_message) TextView errorMessageView;
    @Bind(R.id.sp_addgroup_type) Spinner groupTypeSp;

    @Inject
    APIServicePrivate apiPrivate;
    @Inject
    Bus eventBus;

    //region Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // App.getAppComponent(this).injectAddGroupActivity(this);
        App.getUserComponent(this).injectAddGroupActivity(this);
        setContentView(R.layout.activity_add_group);
        ButterKnife.bind(this);
        eventBus.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }

    //region Event Handling
    @OnClick(R.id.btn_addgroup_create)
    public void addGroup() {
        if (checkUserInputIsNotEmpty()) {
            addGroupRequest();
        }
    }

    //endregion

    private void addGroupRequest() {
        String formatedGroupType = Utils.formatGroupType(groupTypeSp.getSelectedItem().toString());
        Call<BaseResponse> call = apiPrivate.addGroup(
                new AddGroupRequest(
                        addGroupNameEdt.getText().toString(),
                        addGroupDescriptionEdt.getText().toString(),
                        formatedGroupType));

        Timber.i("addGroupRequest");

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Response<BaseResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Timber.i("AddGroup Success");
                    handleSuccess();
                    eventBus.post(new AddedGroupEvent("group added"));
                } else {
                    int statusCode = response.code();
                    Timber.w("addgroup error code = " + statusCode);

                     showErrorMessage(ErrorUtils.parseBaseResponse(response, retrofit));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Timber.i("AddGroup onFailure");
            }
        });
    }

    private void handleSuccess() {
        errorMessageView.setVisibility(View.INVISIBLE);
        this.finish();
    }

    private boolean checkUserInputIsNotEmpty() {
        if (addGroupNameEdt.getText().toString().isEmpty()) {
            addGroupNameEdt.setError(getResources().getString(R.string.addgroup_error_message));
            return false;
        }
        return true;

    }

    private void showErrorMessage(String message) {
        errorMessageView.setText(message);
        errorMessageView.setVisibility(View.VISIBLE);
    }

}
