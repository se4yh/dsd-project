package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.TextView;

import com.dsd.se4yh.activity.CreateRuleActivity;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.data.Property;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.service.configuration.PropertiesResponse;
import com.dsd.se4yh.service.configuration.RulesResponse;
import com.dsd.se4yh.support.ErrorTextMatcher;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;

public class CreateRuleTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<CreateRuleActivity> activityRule = new ActivityTestRule<>(
            CreateRuleActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    @Test
    public void checkCreateRuleView() throws Exception {
        //Response
        String devicesString = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);
        ListDevicesResponse devicesResponse = gson.fromJson(devicesString, ListDevicesResponse.class);

        String rulesString = RestServiceTestHelper.readFileFromAssetManager(RULES_FOR_HEATER, context);
        RulesResponse rulesResponse = gson.fromJson(rulesString, RulesResponse.class);

        String propertiesString = RestServiceTestHelper.readFileFromAssetManager(PROPERTIES_FOR_HEATER, context);
        PropertiesResponse propertiesResponse = gson.fromJson(propertiesString, PropertiesResponse.class);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(devicesString));

        activityRule.launchActivity(new Intent());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(rulesString));

        // select device for spinner
        Device device = devicesResponse.getDevices().get(0);
        onView(withId(R.id.spinner_select_device)).perform(click());
        onView(withText(device.getPrettyName())).perform(click());
        onView(withText(device.getPrettyName())).check(matches(isDisplayed()));

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(propertiesString));

        com.dsd.se4yh.application.data.Rule rule = rulesResponse.getRules().get(0);
        onView(withId(R.id.spinner_select_rule)).check(matches(isDisplayed()));
        onView(withId(R.id.spinner_select_rule)).perform(click());
        onView(withText(rule.getName())).perform(click());
        onView(withId(R.id.spinner_select_rule)).check(matches(isDisplayed()));

        onView(withId(R.id.list_properties)).check(matches(isDisplayed()));

        // check if all properties are displayed
        for (int i = 0; i < propertiesResponse.getProperties().size(); i++) {
            Property property = propertiesResponse.getProperties().get(i);
            onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                    .atPosition(i).onChildView(withId(R.id.tv_list_item_title))
                    .check(matches(withText(property.getName())));
        }
    }

    @Test
    public void actionFailed() {
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        // set error response
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        activityRule.launchActivity(new Intent());

        //Assertion
        onView(withId(R.id.tv_error_view)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void noDevice() {
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(DEVICE_EMPTY_RESPONSE, context);

        // set noDevice response
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));

        activityRule.launchActivity(new Intent());

        //Assertion
        onView(withId(R.id.tv_error_view)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void createRule() {

        final String SET_TIME = "11:22";
        final String SET_TEMP_VALUE = "23";
        final String RULE_NAME = "Rule name";

        //Response
        String devicesString = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);
        ListDevicesResponse devicesResponse = gson.fromJson(devicesString, ListDevicesResponse.class);

        String rulesString = RestServiceTestHelper.readFileFromAssetManager(RULES_FOR_HEATER, context);
        RulesResponse rulesResponse = gson.fromJson(rulesString, RulesResponse.class);

        String propertiesString = RestServiceTestHelper.readFileFromAssetManager(PROPERTIES_FOR_HEATER, context);
        PropertiesResponse propertiesResponse = gson.fromJson(propertiesString, PropertiesResponse.class);

        String ruleCreate = RestServiceTestHelper.readFileFromAssetManager(OK_RESPONSE, context);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(devicesString));

        activityRule.launchActivity(new Intent());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(rulesString));

        // select device for spinner
        Device device = devicesResponse.getDevices().get(0);
        onView(withId(R.id.spinner_select_device)).perform(click());
        onView(withText(device.getPrettyName())).perform(click());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(propertiesString));

        com.dsd.se4yh.application.data.Rule rule = rulesResponse.getRules().get(0);
        onView(withId(R.id.spinner_select_rule)).perform(click());
        onView(withText(rule.getName())).perform(click());

        // set Name
        onView(withId(R.id.et_rule_name)).perform(typeText(RULE_NAME), closeSoftKeyboard());

        // select time when to turn ON
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(0).onChildView(withId(R.id.tv_list_item_dialog))
                .perform(setTextInTextView(SET_TIME));

        // set temperature value
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(1).onChildView(withId(R.id.edt_list_item_number))
                .perform(typeText(SET_TEMP_VALUE), closeSoftKeyboard());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(ruleCreate));

        onView(withId(R.id.action_create_rule)).perform(click());
    }

    @Test
    public void createRuleFailedNoName() {

        final String SET_TIME = "11:22";
        final String SET_TEMP_VALUE = "23";

        //Response
        String devicesString = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);
        ListDevicesResponse devicesResponse = gson.fromJson(devicesString, ListDevicesResponse.class);

        String rulesString = RestServiceTestHelper.readFileFromAssetManager(RULES_FOR_HEATER, context);
        RulesResponse rulesResponse = gson.fromJson(rulesString, RulesResponse.class);

        String propertiesString = RestServiceTestHelper.readFileFromAssetManager(PROPERTIES_FOR_HEATER, context);
        PropertiesResponse propertiesResponse = gson.fromJson(propertiesString, PropertiesResponse.class);

        String ruleCreate = RestServiceTestHelper.readFileFromAssetManager(OK_RESPONSE, context);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(devicesString));

        activityRule.launchActivity(new Intent());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(rulesString));

        // select device for spinner
        Device device = devicesResponse.getDevices().get(0);
        onView(withId(R.id.spinner_select_device)).perform(click());
        onView(withText(device.getPrettyName())).perform(click());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(propertiesString));

        com.dsd.se4yh.application.data.Rule rule = rulesResponse.getRules().get(0);
        onView(withId(R.id.spinner_select_rule)).perform(click());
        onView(withText(rule.getName())).perform(click());

        // select time when to turn ON
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(0).onChildView(withId(R.id.tv_list_item_dialog))
                .perform(setTextInTextView(SET_TIME));

        // set temperature value
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(1).onChildView(withId(R.id.edt_list_item_number))
                .perform(typeText(SET_TEMP_VALUE), closeSoftKeyboard());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(ruleCreate));

        // create a rule
        onView(withId(R.id.action_create_rule)).perform(click());

        // check for error no name
        onView(withId(R.id.et_rule_name)).check(ViewAssertions
                .matches(ErrorTextMatcher.withErrorText(R.string.rule_no_name)));
    }

    @Test
    public void createRuleFailedNoValue() {

        final String SET_TIME = "11:22";
        final String RULE_NAME = "Rule name";

        //Response
        String devicesString = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);
        ListDevicesResponse devicesResponse = gson.fromJson(devicesString, ListDevicesResponse.class);

        String rulesString = RestServiceTestHelper.readFileFromAssetManager(RULES_FOR_HEATER, context);
        RulesResponse rulesResponse = gson.fromJson(rulesString, RulesResponse.class);

        String propertiesString = RestServiceTestHelper.readFileFromAssetManager(PROPERTIES_FOR_HEATER, context);
        PropertiesResponse propertiesResponse = gson.fromJson(propertiesString, PropertiesResponse.class);

        String ruleCreate = RestServiceTestHelper.readFileFromAssetManager(OK_RESPONSE, context);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(devicesString));

        activityRule.launchActivity(new Intent());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(rulesString));

        // select device for spinner
        Device device = devicesResponse.getDevices().get(0);
        onView(withId(R.id.spinner_select_device)).perform(click());
        onView(withText(device.getPrettyName())).perform(click());

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(propertiesString));

        com.dsd.se4yh.application.data.Rule rule = rulesResponse.getRules().get(0);
        onView(withId(R.id.spinner_select_rule)).perform(click());
        onView(withText(rule.getName())).perform(click());

        // set Name
        onView(withId(R.id.et_rule_name)).perform(typeText(RULE_NAME), closeSoftKeyboard());

        // select time when to turn ON
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(0).onChildView(withId(R.id.tv_list_item_dialog))
                .perform(setTextInTextView(SET_TIME));

        // response list properties for selected device
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(ruleCreate));

        // create a rule
        onView(withId(R.id.action_create_rule)).perform(click());

        onView(withText(activityRule.getActivity().getString(R.string.value_must_set)))
                .inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }

    public static ViewAction setTextInTextView(final String value){
        return new ViewAction() {
            @SuppressWarnings("unchecked")
            @Override
            public Matcher<View> getConstraints() {
                return allOf(isDisplayed(), isAssignableFrom(TextView.class));
            }

            @Override
            public void perform(UiController uiController, View view) {
                ((TextView) view).setText(value);
            }

            @Override
            public String getDescription() {
                return "replace text";
            }
        };
    }
}
