package com.dsd.se4yh;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

/**
 * Created by mariusschlinke on 18.12.15.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DevicesTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method
    private NavigationView navigationView;

    @Test
    public void testItemsAreShown() throws Throwable {
        //Response
        String gsonStringDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);
        ListDevicesResponse listDevicesResponse = gson.fromJson(gsonStringDevices, ListDevicesResponse.class);

        // response for Groups Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonStringDevices));

        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_devices, 0);
                navigationView.setCheckedItem(R.id.nav_devices);
            }
        });

        onView(withId(R.id.list)).check(matches(isDisplayed()));

        // check all devices from response
        for (int i = 0; i < listDevicesResponse.getDevices().size(); i++) {
            Device device = listDevicesResponse.getDevices().get(i);
            // check if title in list is equal to device pretty name
            onData(is(instanceOf(Device.class))).inAdapterView(withId(R.id.list))
                    .atPosition(i).onChildView(withId(R.id.tv_item_title))
                    .check(matches(withText(device.getPrettyName())));
        }
    }

    @Test
    public void testListDeviceError() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        // response for Groups Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        activityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_devices, 0);
                navigationView.setCheckedItem(R.id.nav_devices);
            }
        });

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void testEmptyDeviceList() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(DEVICE_EMPTY_RESPONSE, context);

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        // response for Groups Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));


        activityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_devices, 0);
                navigationView.setCheckedItem(R.id.nav_devices);
            }
        });

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }


}
