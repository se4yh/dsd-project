package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;

import com.dsd.se4yh.activity.DeviceDetailsActivity;
import com.dsd.se4yh.adapter.DeviceDetailsAdapter;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.DeviceDetailsResponse;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

public class ChangeDeviceStatus extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<DeviceDetailsActivity> activityRule = new ActivityTestRule<>(
            DeviceDetailsActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method


    @Test
    public void preformActionSwitch() throws Throwable {
        //Response
        String gsonStringDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICE_DETAILS_RESPONSE, context);
        DeviceDetailsResponse deviceDetailsResponse = gson.fromJson(gsonStringDevices, DeviceDetailsResponse.class);

        // response for Groups Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonStringDevices));

        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, 1);
        activityRule.launchActivity(intent);

        String state = deviceDetailsResponse.getDevice().getItems().get(0).getValue();
        boolean turnOn = state.equals(Constants.SWITCH_ON);

        // change switch
        onData(is(instanceOf(DeviceDetailsAdapter.DeviceDescription.class))).inAdapterView(withId(R.id.list))
                .atPosition(3).onChildView(withId(R.id.sw_list_item_switch))
                .perform(click());

        // TODO: not a good solution
    }

    @Test
    public void preformActionSetTemp() throws Throwable {
        final String SET_TEMP = "77";

        //Response
        String gsonStringDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICE_DETAILS_RESPONSE, context);
        DeviceDetailsResponse deviceDetailsResponse = gson.fromJson(gsonStringDevices, DeviceDetailsResponse.class);

        // response
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonStringDevices));


        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, 1);
        activityRule.launchActivity(intent);

        // set temperature
        onData(is(instanceOf(DeviceDetailsAdapter.DeviceDescription.class))).inAdapterView(withId(R.id.list))
                .atPosition(5).onChildView(withId(R.id.edt_list_item_number))
                .perform(clearText(), typeText(SET_TEMP), closeSoftKeyboard());

        // check the set temperature
        onData(is(instanceOf(DeviceDetailsAdapter.DeviceDescription.class))).inAdapterView(withId(R.id.list))
                .atPosition(5).onChildView(withId(R.id.edt_list_item_number))
                .check(matches(withText(SET_TEMP)));

        // set response with new temperature
        deviceDetailsResponse.getDevice().getItems().get(2).setValue(SET_TEMP);
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gson.toJson(deviceDetailsResponse)));

        // send data
        onView(withId(R.id.action_send)).check(matches(isDisplayed()));
        onView(withId(R.id.action_send)).perform(click());

        // check new data
        onData(is(instanceOf(DeviceDetailsAdapter.DeviceDescription.class))).inAdapterView(withId(R.id.list))
                .atPosition(5).onChildView(withId(R.id.edt_list_item_number))
                .check(matches(withText(SET_TEMP)));
    }

    @Test
    public void actionFailed() {
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        activityRule.launchActivity(new Intent());

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }

}
