package com.dsd.se4yh;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.LoginActivity;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.support.ActivityRule;
import com.dsd.se4yh.support.ErrorTextMatcher;
import com.dsd.se4yh.support.MainActivityIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by mariusschlinke on 24.11.15.
 */


/*
 Test Annotations
 @SmallTests
 Execution time: < 100ms
 should runs in an isolated environment and use mock objects for external dependencies

 @MediumTests
 Execution time: < 2s
 e.g. Resource access to the file system through well defined interfaces
 like databases, ContentProviders, or Context is permitted.

 @LargeTests
 Execution time: < 120s
 e.g. UI tests, that test across multiple components.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginTest {

    public static final String LOGIN_TEST_DATA = "test@test.com";
    public static final String PASSWORD_TEST_DATA = "test";

    public static final String LOGIN_INVALID_TEST_DATA = "323@gmail.com";
    public static final String PASSWORD_INVALID_TEST_DATA = "123456";

    @Rule
    public final ActivityRule<LoginActivity> mActivityRule = new ActivityRule<>(LoginActivity.class);
    private MainActivityIdlingResource mainActivityIdlingResource;

    @After
    public void tearDown() {
        unregisterIdlingResources(mainActivityIdlingResource);
        mainActivityIdlingResource.unsubscribe();
    }

    @Before
    public void setUp() {
        mainActivityIdlingResource = new MainActivityIdlingResource();
        App.activityEventStream()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainActivityIdlingResource);
    }

    @Test
    public void testValidLogin() {
        // Type in login
        onView(withId(R.id.edt_login_name)).perform(clearText(), typeText(LOGIN_TEST_DATA));
        // Type in password
        onView(withId(R.id.edt_login_password)).perform(typeText(PASSWORD_TEST_DATA), closeSoftKeyboard());
        // Press Login Button
        onView(withId(R.id.btn_login_login)).perform(click());

        // Wait until MainActivity is created
        registerIdlingResources(mainActivityIdlingResource);
    }

    @Test
    public void testInvalidLogin() {
        // Type in invalid login
        onView(withId(R.id.edt_login_name)).perform(clearText(), typeText(LOGIN_INVALID_TEST_DATA));
        // Type in invalid password
        onView(withId(R.id.edt_login_password)).perform(typeText(PASSWORD_INVALID_TEST_DATA), closeSoftKeyboard());
        // Press Login Button
        onView(withId(R.id.btn_login_login)).perform(click());
        //Check if error text appeared
        onView(withId(R.id.tv_login_error_message)).check(matches(isDisplayed()));
    }

    @Test
    public void testEmptyLogin() {
        // clear text otherwise the login will happen
        onView(withId(R.id.edt_login_name)).perform(clearText());
        // Type in password
        onView(withId(R.id.edt_login_password)).perform(clearText(), typeText(PASSWORD_TEST_DATA), closeSoftKeyboard());
        // Press Login Button
        onView(withId(R.id.btn_login_login)).perform(click());
        // Check if error text is displayed
        onView(withId(R.id.edt_login_name)).check(ViewAssertions.matches(ErrorTextMatcher.withErrorText(R.string.login_error_empty_login)));
    }

    @Test
    public void testEmptyPassword() {
        // Type in login
        onView(withId(R.id.edt_login_name)).perform(clearText(), typeText(LOGIN_TEST_DATA), closeSoftKeyboard());
        // Press Login Button
        onView(withId(R.id.btn_login_login)).perform(click());
        // Check if error text is displayed
        onView(withId(R.id.edt_login_password)).check(ViewAssertions.matches(ErrorTextMatcher.withErrorText(R.string.login_error_empty_password)));
    }
}
