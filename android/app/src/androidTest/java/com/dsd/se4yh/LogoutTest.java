package com.dsd.se4yh;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.dsd.se4yh.activity.LoginActivity;
import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.support.LoginActivityIdlingResource;
import com.dsd.se4yh.support.MainActivityIdlingResource;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LogoutTest extends BasicTest implements ResponseFileConstants {

    private NavigationView navigationView;


    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method
    private LoginActivityIdlingResource loginActivityIdlingResource;

    @Before
    public void setup() throws Exception{
        super.setUp();
        loginActivityIdlingResource = new LoginActivityIdlingResource();
        App.activityEventStream()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(loginActivityIdlingResource);

    }

    @After
    public void tearDown() {
        unregisterIdlingResources(loginActivityIdlingResource);
        loginActivityIdlingResource.unsubscribe();
    }

    @Test
    public void testLogout() throws Exception {

        //Response
        String gsonStringOK = RestServiceTestHelper.readFileFromAssetManager(OK_RESPONSE, context);
        BaseResponse baseResponse = gson.fromJson(gsonStringOK, BaseResponse.class);

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.list_refresh_layout)).check(ViewAssertions.matches(isDisplayed()));
        navigationView.getMenu().performIdentifierAction(R.id.nav_logout, 0);
        navigationView.setCheckedItem(R.id.nav_logout);

        // Request for Logout
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(baseResponse.getCode()))
                .setBody(gsonStringOK));

        // Wait until MainActivity is created
        registerIdlingResources(loginActivityIdlingResource);

        //Assertion
        onView(withId(R.id.btn_login_login)).check(ViewAssertions.matches(isDisplayed()));

    }


    @Test
    public void testFailedLogout() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        //Request for getGroups
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.list_refresh_layout)).check(ViewAssertions.matches(isDisplayed()));
        navigationView.getMenu().performIdentifierAction(R.id.nav_logout, 0);
        navigationView.setCheckedItem(R.id.nav_logout);

        // Request for Logout
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        //Assertion
        onView(withId(R.id.btn_login_login)).check(ViewAssertions.matches(isDisplayed()));
    }
}
