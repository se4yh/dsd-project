package com.dsd.se4yh;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.ResetPasswordActivity;
import com.dsd.se4yh.support.ErrorTextMatcher;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by mariusschlinke on 24.11.15.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ResetPasswordTest {

    public static final String LOGIN_TEST_DATA = "test@gmail.com";
    public static final String LOGIN_INVALID_TEST_DATA = "123@gmail.com";

    @Rule
    public ActivityTestRule<ResetPasswordActivity> mActivityRule = new ActivityTestRule<>(ResetPasswordActivity.class);

    // currently always failing, need server validation of user email
    @Test
    public void testValidReset() {
        // Type in login
        onView(withId(R.id.edt_reset_password_email)).perform(typeText(LOGIN_TEST_DATA));
        // Press Login Button
        onView(withId(R.id.btn_reset_password_send)).perform(click());
        //Check if no error appeared
        //TODO: refactor to view assertion that matches with the response from the server
    }

    @Test
    public void testInvalidReset() {
        // Type in login
        onView(withId(R.id.edt_reset_password_email)).perform(typeText(LOGIN_INVALID_TEST_DATA));
        // Press Login Button
        onView(withId(R.id.btn_reset_password_send)).perform(click());
        //Check if no error appeared
        onView(withId(R.id.tv_reset_password_error_message)).check(matches(isDisplayed()));
    }

    @Test
    public void testEmptyInput() {
        // Press Login Button
        onView(withId(R.id.btn_reset_password_send)).perform(click());
        // Check if error text is displayed
        onView(withId(R.id.edt_reset_password_email)).check(ViewAssertions.matches(ErrorTextMatcher.withErrorText(R.string.resetpassword_empty_login)));
    }
}
