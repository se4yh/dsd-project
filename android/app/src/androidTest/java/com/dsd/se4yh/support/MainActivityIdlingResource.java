package com.dsd.se4yh.support;

import android.support.test.espresso.IdlingResource;

import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.ActivityEvent;
import com.dsd.se4yh.application.ActivityEventKind;

import rx.Subscriber;

/**
 * Created by mariusschlinke on 05.12.15.
 */
public class MainActivityIdlingResource extends Subscriber<ActivityEvent> implements IdlingResource {
    private volatile ResourceCallback resourceCallback;
    private volatile boolean mainActivityCreated;

    @Override
    public String getName() {
        return "MainActivity Created";
    }

    @Override
    public boolean isIdleNow() {
        return mainActivityCreated;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onNext(ActivityEvent activityEvent) {
        if(mainActivityCreated(activityEvent)) {
            mainActivityCreated = true;
            resourceCallback.onTransitionToIdle();
        }
    }

    private boolean mainActivityCreated(ActivityEvent activityEvent) {
        return activityEvent.getActivityClass().equals(MainActivity.class) && activityEvent.getEventKind() == ActivityEventKind.CREATED;
    }
}
