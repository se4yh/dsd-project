package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import com.dsd.se4yh.activity.CreateRuleActivity;
import com.dsd.se4yh.application.Utils;
import com.dsd.se4yh.application.data.Property;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.RuleResponse;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

public class EditRuleTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<CreateRuleActivity> activityRule = new ActivityTestRule<>(
            CreateRuleActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    private void startActivity(int id) {
        Intent intent = new Intent();
        intent.putExtra(CreateRuleActivity.EXTRA_RULE_ID, id);
        activityRule.launchActivity(intent);
    }

    @Test
    public void createRuleFailed() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        // response unauthorized
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        startActivity(1);

        onView(withId(R.id.tv_error_view)).check(matches(isDisplayed()));
    }

    @Test
    public void getRule() throws Exception {
        //Response
        String rulesString = RestServiceTestHelper.readFileFromAssetManager(EDIT_RULE_RESPONSE, context);
        RuleResponse ruleResponse = gson.fromJson(rulesString, RuleResponse.class);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(rulesString));

        startActivity(ruleResponse.getRule().getId());

        // check if name is displayed
        onView(withId(R.id.et_rule_name)).check(matches(withText(ruleResponse.getRule().getName())));

        // check for the list of properties
        onView(withId(R.id.list_properties)).check(matches(isDisplayed()));

        // get time format
        Property property = ruleResponse.getRule().getProperties().get(0);
        String[] values = property.getValue().split(" ");
        int minute = Integer.valueOf(values[0]);
        int hour = Integer.valueOf(values[1]);
        String time = Utils.getTimeString(hour, minute);

        // check property at position 0
        onData(is(instanceOf(Property.class))).inAdapterView(withId(R.id.list_properties))
                .atPosition(0).onChildView(withId(R.id.tv_list_item_dialog))
                .check(matches(withText(time)));
    }

    @Test
    public void noIdOfRuleEdit() {
        //Response
        String devicesString = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);

        // response list all devices
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(devicesString));

        // just start activity without rule
        activityRule.launchActivity(new Intent());

        // check if name is not displayed
        onView(withId(R.id.et_rule_name)).check(matches(not(isDisplayed())));

        // check for spinner to select device
        onView(withId(R.id.spinner_select_device)).check(matches(isDisplayed()));
    }

}
