package com.dsd.se4yh.support;

import android.support.test.espresso.IdlingResource;

import com.dsd.se4yh.activity.GroupDetailsActivity;
import com.dsd.se4yh.application.ActivityEvent;
import com.dsd.se4yh.application.ActivityEventKind;

import rx.Subscriber;

/**
 * Created by mariusschlinke on 05.01.16.
 */
public class GroupDetailsIdlingResource extends Subscriber<ActivityEvent> implements IdlingResource {
    private volatile ResourceCallback resourceCallback;
    private volatile boolean groupDetailsActivityCreated;

    @Override
    public String getName() {
        return "GroupDetailsActivity Created";
    }

    @Override
    public boolean isIdleNow() {
        return groupDetailsActivityCreated;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onNext(ActivityEvent activityEvent) {
        if (groupDetailsActivityCreated(activityEvent)) {
            groupDetailsActivityCreated = true;
            resourceCallback.onTransitionToIdle();
        }
    }

    private boolean groupDetailsActivityCreated(ActivityEvent activityEvent) {
        return activityEvent.getActivityClass().equals(GroupDetailsActivity.class) && activityEvent.getEventKind() == ActivityEventKind.CREATED;
    }
}
