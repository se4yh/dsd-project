package com.dsd.se4yh.support;

import android.support.test.espresso.IdlingResource;

import com.dsd.se4yh.activity.AssignDeviceActivity;
import com.dsd.se4yh.application.ActivityEvent;
import com.dsd.se4yh.application.ActivityEventKind;

import rx.Subscriber;

/**
 * Created by mariusschlinke on 05.01.16.
 */
public class AssignDeviceActivityIdlingResource extends Subscriber<ActivityEvent> implements IdlingResource {
    private volatile ResourceCallback resourceCallback;
    private volatile boolean assignDeviceActivityCreated;

    @Override
    public String getName() {
        return "AssignDeviceActivity Created";
    }

    @Override
    public boolean isIdleNow() {
        return assignDeviceActivityCreated;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onNext(ActivityEvent activityEvent) {
        if (assignDeviceActivityCreated(activityEvent)) {
            assignDeviceActivityCreated = true;
            resourceCallback.onTransitionToIdle();
        }
    }

    private boolean assignDeviceActivityCreated(ActivityEvent activityEvent) {
        return activityEvent.getActivityClass().equals(AssignDeviceActivity.class) && activityEvent.getEventKind() == ActivityEventKind.CREATED;
    }
}