package com.dsd.se4yh;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by mariusschlinke on 19.12.15.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class GroupsTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    @Test
    public void testEmptyList() {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(GROUP_EMPTY_RESPONSE, context);

        activityRule.launchActivity(new Intent());

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void testListGroup() throws Exception {
        //Response
        String deviceResponse = RestServiceTestHelper.readFileFromAssetManager(GROUP_RESPONSE, context);

        activityRule.launchActivity(new Intent());

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(deviceResponse));

        //Assertion
        onView(withId(R.id.list_refresh_layout)).check(ViewAssertions.matches(isDisplayed()));

    }

    @Test
    public void testListGroupError() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);
        activityRule.launchActivity(new Intent());

        //Request for getGroup from MainActivity onStart
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }


}
