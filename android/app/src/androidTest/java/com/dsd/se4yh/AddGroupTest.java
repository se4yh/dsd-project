package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.AddGroupActivity;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.AddGroupRequest;
import com.dsd.se4yh.service.configuration.AddGroupResponse;
import com.dsd.se4yh.service.configuration.BaseResponse;
import com.dsd.se4yh.support.ErrorTextMatcher;
import com.dsd.se4yh.support.MainActivityIdlingResource;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by mariusschlinke on 07.12.15.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddGroupTest extends BasicTest implements ResponseFileConstants {

    private static final String ADD_GROUP_NAME_TEST_DATA = "TESTGROUP";
    private static final String ADD_GROUP_DESCRIPTION_TEST_DATA = "TESTDESCRIPTION";
    @Rule
    public final ActivityTestRule<AddGroupActivity> mActivityRule = new ActivityTestRule<>(
            AddGroupActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method
    private long rand;


    @Before
    public void setUp() throws Exception {
        super.setUp();
        rand = System.currentTimeMillis() % 100;
    }


    @Test
    public void testAddGroup() throws Exception {
        //Response
        String addGroupResponse = RestServiceTestHelper.readFileFromAssetManager(ADD_GROUP_OK, context);

        mActivityRule.launchActivity(new Intent());

        //Request
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(addGroupResponse));

        //View interaction
        // type in test name
        onView(withId(R.id.edt_addgroup_groupname)).perform(clearText(), typeText(ADD_GROUP_NAME_TEST_DATA + String.valueOf(rand)));
        // click create button
        onView(withId(R.id.btn_addgroup_create)).perform(click());

        //TODO: add meaningful assertion like check if mainactivity is shown
    }

    @Test
    public void testAddGroupWithDescription() throws Exception {
        //Response
        String gsonStringOK = RestServiceTestHelper.readFileFromAssetManager(OK_RESPONSE, context);
        BaseResponse baseResponse = gson.fromJson(gsonStringOK, BaseResponse.class);

        mActivityRule.launchActivity(new Intent());

        //View interaction
        // type in test name
        onView(withId(R.id.edt_addgroup_groupname)).perform(clearText(), typeText(ADD_GROUP_NAME_TEST_DATA + String.valueOf(rand)));
        // type in description
        onView(withId(R.id.edt_addgroup_groupdescription)).perform(clearText(), typeText(ADD_GROUP_DESCRIPTION_TEST_DATA + String.valueOf(rand)));
        // click create button
        onView(withId(R.id.btn_addgroup_create)).perform(click());

        //Request
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(baseResponse.getCode()))
                .setBody(gsonStringOK));
    }

    @Test
    public void testWrongAuthorization() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        mActivityRule.launchActivity(new Intent());

        //Request
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        //View interaction
        // type in test name
        onView(withId(R.id.edt_addgroup_groupname)).perform(clearText(),
                typeText(ADD_GROUP_NAME_TEST_DATA + String.valueOf(rand)));
        // click create button
        onView(withId(R.id.btn_addgroup_create)).perform(click());

        //Assertion
        onView(withId(R.id.tv_addgroup_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }


    @Test
    public void testGroupExist() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ADD_GROUP_ALREADY_EXISTS_ERROR, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        mActivityRule.launchActivity(new Intent());

        //Request
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        //View interaction
        // type in test name
        onView(withId(R.id.edt_addgroup_groupname)).perform(clearText(),
                typeText(ADD_GROUP_NAME_TEST_DATA + String.valueOf(rand)));
        // click create button
        onView(withId(R.id.btn_addgroup_create)).perform(click());

        //Assertion
        onView(withId(R.id.tv_addgroup_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void testEmptyName() {
        mActivityRule.launchActivity(new Intent());
        // click create button
        onView(withId(R.id.btn_addgroup_create)).perform(click());
        // Check if error text is displayed
        onView(withId(R.id.edt_addgroup_groupname)).check(ViewAssertions.matches(ErrorTextMatcher.withErrorText(R.string.addgroup_error_message)));
    }


}
