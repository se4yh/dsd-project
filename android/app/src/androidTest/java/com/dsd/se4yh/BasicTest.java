package com.dsd.se4yh;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.component.DaggerUserComponent;
import com.dsd.se4yh.application.component.UserComponent;
import com.dsd.se4yh.application.data.User;
import com.dsd.se4yh.application.module.UserModule;
import com.dsd.se4yh.service.configuration.APIConstants;
import com.google.gson.Gson;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.Before;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public abstract class BasicTest {

    protected App application;

    protected MockWebServer server;
    protected Gson gson;

    //We need this for accessing the assets in the test package
    protected Instrumentation instrumentation;
    protected Context context;

    @Before
    public void setUp() throws Exception {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        context = instrumentation.getContext();
        application = (App) instrumentation.getTargetContext().getApplicationContext();

        /* mock server setup */
        server = new MockWebServer();
        server.start();
        APIConstants.BASE_URL = server.url("/").toString();

        UserComponent component = DaggerUserComponent.builder()
                .userModule(new UserModule(createUser(), createToken(), getUrl()))
                .appComponent(App.getAppComponent(application))
                .build();
        application.setUserComponent(component);
        gson = App.getAppComponent(application).gson();
    }

    private User createUser() {
        User user = new User();
        user.setId("1");
        user.setName("test");
        user.setEmail("test@test.com");
        return user;
    }

    private String createToken() {
        return "test124124";
    }

    private String getUrl() {
        return APIConstants.BASE_URL;
    }
}
