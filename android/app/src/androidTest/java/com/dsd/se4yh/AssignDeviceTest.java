package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.AssignDeviceActivity;
import com.dsd.se4yh.activity.GroupDetailsActivity;
import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.App;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.application.data.Device;
import com.dsd.se4yh.application.data.Group;
import com.dsd.se4yh.service.configuration.ListDevicesResponse;
import com.dsd.se4yh.service.configuration.ListGroupsResponse;
import com.dsd.se4yh.support.AssignDeviceActivityIdlingResource;
import com.dsd.se4yh.support.GroupDetailsIdlingResource;
import com.dsd.se4yh.support.MainActivityIdlingResource;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.click;

/**
 * Created by mariusschlinke on 05.01.16.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AssignDeviceTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public final ActivityTestRule<GroupDetailsActivity> mActivityRule = new ActivityTestRule<>(
            GroupDetailsActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    
    @Test
    public void testAssignDevice() throws Exception {
        String gsonGroup = RestServiceTestHelper.readFileFromAssetManager(GROUP_RESPONSE, context);
        String gsonDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);

        ListGroupsResponse listGroupsResponse = gson.fromJson(gsonGroup, ListGroupsResponse.class);

        //Request for getGroups GroupDetailsActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonDevices));

        //Request for getGroups AssignDeviceActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonDevices));

        Intent intent = new Intent();
        Group group = listGroupsResponse.getGroups().get(0);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_ID, group.getId());
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_NAME, group.getName());
        mActivityRule.launchActivity(intent);



        //Request for assignDevice AssignDeviceActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        // open AssignDeviceActivity
        onView(withId(R.id.fab)).perform(click());

        //perform long click on item
        onData(is(instanceOf(Device.class))).inAdapterView(withId(R.id.list))
                .atPosition(0).onChildView(withId(R.id.tv_item_title)).perform(longClick());
        //select first device
        onData(is(instanceOf(Device.class))).inAdapterView(withId(R.id.list))
                .atPosition(0).onChildView(withId(R.id.cb_checkable)).perform(click());

        //click on action item
        onView(withId(R.id.icon_confirm)).perform(click());

    }


    @Test
    public void testNoAssignment() {
        String gsonGroup = RestServiceTestHelper.readFileFromAssetManager(GROUP_RESPONSE, context);
        String gsonDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICES_RESPONSE, context);

        ListGroupsResponse listGroupsResponse = gson.fromJson(gsonGroup, ListGroupsResponse.class);

        //Request for getDevices GroupDetails
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonDevices));

        Intent intent = new Intent();
        Group group = listGroupsResponse.getGroups().get(0);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_ID, group.getId());
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_NAME, group.getName());
        mActivityRule.launchActivity(intent);

        //Request for assignDevice AssignDeviceActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonDevices));

        // open AssignDeviceActivity
        onView(withId(R.id.fab)).perform(click());

        //perform long click on item
        onData(is(instanceOf(Device.class))).inAdapterView(withId(R.id.list))
                .atPosition(0).onChildView(withId(R.id.tv_item_title)).perform(longClick());
        //click on action item
        onView(withId(R.id.icon_confirm)).perform(click());

        //check that action item is not displayed
        onView(withId(R.id.icon_confirm)).check(doesNotExist());


    }


    @Test
    public void testNoDevices() {
        String gsonGroup = RestServiceTestHelper.readFileFromAssetManager(GROUP_RESPONSE, context);
        String gsonDevices = RestServiceTestHelper.readFileFromAssetManager(DEVICE_EMPTY_RESPONSE, context);

        ListGroupsResponse listGroupsResponse = gson.fromJson(gsonGroup, ListGroupsResponse.class);

        //Request for getGroups AssignDeviceActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonDevices));

        Intent intent = new Intent();
        Group group = listGroupsResponse.getGroups().get(0);
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_ID, group.getId());
        intent.putExtra(Constants.INTENT_EXTRA_GROUP_NAME, group.getName());
        mActivityRule.launchActivity(intent);

        //Request for assignDevice AssignDeviceActivity
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        // open AssignDeviceActivity
        onView(withId(R.id.fab)).perform(click());

        //check that that info message is shown
        onView(withId(R.id.tv_assign_device_error_message)).check(matches(isDisplayed()));
    }

}
