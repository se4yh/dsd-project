package com.dsd.se4yh.support;

/**
 * Created by mariusschlinke on 17.12.15.
 */
public interface ResponseFileConstants {

    /* general response */
    String ERROR_401_RESPONSE = "error_401.json";
    String OK_RESPONSE = "ok_200.json";

    /** {@link com.dsd.se4yh.activity.AddGroupActivity} */
    String ADD_GROUP_OK = "add_group_response.json";
    String ADD_GROUP_ALREADY_EXISTS_ERROR = "group_error_404.json";

    /** {@link com.dsd.se4yh.fragment.GroupFragment } */
    String GROUP_EMPTY_RESPONSE = "group_empty_list.json";
    String GROUP_RESPONSE = "group_response.json";
    String GROUPS_RESPONSE = "groups_response.json";

    /** {@link com.dsd.se4yh.activity.DeviceDetailsActivity} */
    String CANNOT_SHOW_DEVICES_DETAILS_ERROR = "device_details_error_404.json";

    /** {@link com.dsd.se4yh.fragment.DevicesFragment} */
    String DEVICE_RESPONSE = "device_response.json";
    String DEVICE_EMPTY_RESPONSE = "device_empty_list.json";
    String DEVICES_RESPONSE = "devices_response.json";

    /** {@link com.dsd.se4yh.activity.DeviceDetailsActivity} */
    String DEVICE_DETAILS_RESPONSE = "device_details_response.json";

    /** {@link com.dsd.se4yh.activity.GroupDetailsActivity} */
    String GROUP_DETAILS_RESPONSE = "group_details_response.json";

    /** {@link com.dsd.se4yh.fragment.RuleFragment} */
    String RULES_LIST_RESPONSE = "rules_list_response.json";
    String RULES_EMPTY_LIST = "rules_empty_list.json";

    /** {@link com.dsd.se4yh.activity.CreateRuleActivity} */
    String EDIT_RULE_RESPONSE = "edit_rule_turn_on_at.json";
    String RULES_FOR_HEATER = "rules_for_device_heater_response.json";
    String PROPERTIES_FOR_HEATER = "rule_heater_turn_on_at.json";
}
