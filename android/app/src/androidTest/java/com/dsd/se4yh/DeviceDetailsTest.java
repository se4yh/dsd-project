package com.dsd.se4yh;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.dsd.se4yh.activity.DeviceDetailsActivity;
import com.dsd.se4yh.application.Constants;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.DeviceDetailsResponse;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Created by Chape on 2015-12-18.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class DeviceDetailsTest extends BasicTest implements ResponseFileConstants {

    @Rule
    public ActivityTestRule<DeviceDetailsActivity> activityRule = new ActivityTestRule<>(
            DeviceDetailsActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method


    @Test
    public void getDeviceDetailsTestFailed() throws Exception{
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(CANNOT_SHOW_DEVICES_DETAILS_ERROR, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);


        //Request for getDeviceDetails
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, 1);
        activityRule.launchActivity(intent);


        onView(withId(R.id.list_error_message)).check(matches(isDisplayed()));
    }


    @Test
    public void testShowDeviceDetails() throws Exception {
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(DEVICE_DETAILS_RESPONSE, context);
        DeviceDetailsResponse deviceDetailsResponse = gson.fromJson(gsonString, DeviceDetailsResponse.class);
        String expectedDeviceName = deviceDetailsResponse.getDevice().getPrettyName();
        String expectedRoomTemperatureValue = deviceDetailsResponse.getDevice().getItems().get(1).getValue();

        //Request for getDeviceDetails
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));

        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, 1);
        activityRule.launchActivity(intent);

        //assert that name is displayed correctly
        onView(withText(expectedDeviceName)).check(matches(isDisplayed()));

        //assert that switch value is OFF
        onView(withText(expectedRoomTemperatureValue)).check(matches(isDisplayed()));

    }


    @Test
    public void testSwitch() {
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(DEVICE_DETAILS_RESPONSE, context);

        //Request for getDeviceDetails
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));

        Intent intent = new Intent();
        intent.putExtra(Constants.INTENT_EXTRA_DEVICE_ID, 1);
        activityRule.launchActivity(intent);

        onView(withId(R.id.sw_list_item_switch)).perform(click());

        //click on action item
        onView(withId(R.id.action_send)).perform(click());

    }

}
