package com.dsd.se4yh.support;

import android.support.test.espresso.IdlingResource;


import com.dsd.se4yh.activity.LoginActivity;
import com.dsd.se4yh.application.ActivityEvent;
import com.dsd.se4yh.application.ActivityEventKind;

import rx.Subscriber;

/**
 * Created by mariusschlinke on 17.12.15.
 */
public class LoginActivityIdlingResource extends Subscriber<ActivityEvent> implements IdlingResource {
    private volatile ResourceCallback resourceCallback;
    private volatile boolean loginActivityCreated;

    @Override
    public String getName() {
        return "LoginActivity Created";
    }

    @Override
    public boolean isIdleNow() {
        return loginActivityCreated;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onNext(ActivityEvent activityEvent) {
        if(loginActivityCreated(activityEvent)) {
            loginActivityCreated = true;
            resourceCallback.onTransitionToIdle();
        }
    }

    private boolean loginActivityCreated(ActivityEvent activityEvent) {
        return activityEvent.getActivityClass().equals(LoginActivity.class) && activityEvent.getEventKind() == ActivityEventKind.CREATED;
    }
}