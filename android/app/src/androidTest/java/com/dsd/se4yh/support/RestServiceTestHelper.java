package com.dsd.se4yh.support;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mariusschlinke on 08.12.15.
 */
public class RestServiceTestHelper {

    public static String readFileFromAssetManager(String fileName, Context context) {
        AssetManager assetManager = context.getResources().getAssets();
        try {
            InputStream inputStream = assetManager.open(fileName);
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String text = new String(buffer);

            return text;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }



}