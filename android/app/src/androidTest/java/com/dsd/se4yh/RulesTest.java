package com.dsd.se4yh;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;

import com.dsd.se4yh.activity.MainActivity;
import com.dsd.se4yh.application.data.RuleList;
import com.dsd.se4yh.service.configuration.APIError;
import com.dsd.se4yh.service.configuration.ListRulesResponse;
import com.dsd.se4yh.support.ResponseFileConstants;
import com.dsd.se4yh.support.RestServiceTestHelper;
import com.squareup.okhttp.mockwebserver.MockResponse;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

public class RulesTest extends BasicTest implements ResponseFileConstants {

    private NavigationView navigationView;

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(
            MainActivity.class,
            true,     // initialTouchMode
            false);   // launchActivity. False so we can customize the intent per test method

    @Test
    public void testEmptyListRules() {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(RULES_EMPTY_LIST, context);

        // response for main Group Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        // set request with empty list
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonString));

        // go to Rule Fragment
        activityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_rules, 0);
                navigationView.setCheckedItem(R.id.nav_rules);
            }
        });

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void testListRules() throws Throwable {
        //Response
        String gsonStringRules = RestServiceTestHelper.readFileFromAssetManager(RULES_LIST_RESPONSE, context);
        ListRulesResponse listRulesResponse = gson.fromJson(gsonStringRules, ListRulesResponse.class);

        // response for main Group Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        // set response with list of rules
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(gsonStringRules));

        // go to Rule Fragment
        activityRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_rules, 0);
                navigationView.setCheckedItem(R.id.nav_rules);
            }
        });

        onView(withId(R.id.list)).check(matches(isDisplayed()));

        // check all devices from response
        for (int i = 0; i < listRulesResponse.getRules().size(); i++) {
            RuleList rule = listRulesResponse.getRules().get(i);
            onData(is(instanceOf(RuleList.class))).inAdapterView(withId(R.id.list))
                    .atPosition(i).onChildView(withId(R.id.tv_rule_title))
                    .check(matches(withText(rule.getRuleName())));
            onData(is(instanceOf(RuleList.class))).inAdapterView(withId(R.id.list))
                    .atPosition(i).onChildView(withId(R.id.tv_rule_subtitle))
                    .check(matches(withText(rule.getDeviceName())));
        }
    }

    @Test
    public void testListRulesError() throws Exception {
        //Response
        String gsonString = RestServiceTestHelper.readFileFromAssetManager(ERROR_401_RESPONSE, context);
        APIError errorResponse = gson.fromJson(gsonString, APIError.class);

        // response for main Group Fragment
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(""));

        activityRule.launchActivity(new Intent());
        navigationView = (NavigationView) activityRule.getActivity().findViewById(R.id.nav_view);

        //View interaction
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());

        // set error response
        server.enqueue(new MockResponse()
                .setResponseCode(Integer.valueOf(errorResponse.getCode()))
                .setBody(gsonString));

        // go to Rule Fragment
        activityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigationView.getMenu().performIdentifierAction(R.id.nav_rules, 0);
                navigationView.setCheckedItem(R.id.nav_rules);
            }
        });

        //Assertion
        onView(withId(R.id.list_error_message)).check(ViewAssertions.matches(isDisplayed()));
    }
}
