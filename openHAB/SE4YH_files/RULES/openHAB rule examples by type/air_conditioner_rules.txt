import org.openhab.core.library.types.*
import org.openhab.core.persistence.*
import org.openhab.model.script.actions.*    

var boolean power_enable = true
var Timer timer



//s7rule
rule "klima turn on at"
when
    Time cron "0 40 8 * * ?" 
then
    if (power_enable == true  ){
        living_room_ac__air_conditioner__power.sendCommand(ON)
        living_room_ac__air_conditioner__tempset.sendCommand(22)
    }
end
//e7rule
//s9rule
rule "klima turn off at"
when
    Time cron "0 45 9 * * ?" 
then
    living_room_ac__air_conditioner__power.sendCommand(OFF)
end
//e9rule
//s12rule
rule "klima turn off if room temp is lower than"
when
	Time cron "*/5 * * * * ?"
then
	if(living_room_ac__air_conditioner__tempin.state < 25){ 
             if(living_room_ac__air_conditioner__power.state != OFF){
                 living_room_ac__air_conditioner__power.sendCommand(OFF)
              }
       }
end
//e12rule

//s13rule
rule "klima turn off if window is open"
when
   Item living_room_window__contact__contact changed 
then 
     if (living_room_window__contact__contact.state == CLOSED) {	 
        power_enable = false	 
	 living_room_ac__air_conditioner__power.sendCommand(OFF)
      }else{
	power_enable = true  
      } 
end
//e13rule
//s16rule
rule "klima turn on if room temp is higher than"
when
	Time cron "*/5 * * * * ?"
then
	if(living_room_ac__air_conditioner__tempin.state > 18){ 
             if(power_enable == true){
                 if(living_room_ac__air_conditioner__power.state != ON){
                    living_room_ac__air_conditioner__power.sendCommand(ON)
                 }
             }
       }
end
//e16rule
