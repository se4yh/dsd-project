-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Računalo: 127.0.0.1
-- Vrijeme generiranja: Sij 11, 2016 u 03:58 
-- Verzija poslužitelja: 5.5.27
-- PHP verzija: 5.6.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza podataka: `dsd_projekt`
--

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Izbacivanje podataka za tablicu `properties`
--

INSERT INTO `properties` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(3, 'select time when to turn ON', 'TIME', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'set temperature value', 'NUMBER', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'enable', 'SWITCH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Insert number of seconds', 'NUMBER', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `property_rule`
--

CREATE TABLE IF NOT EXISTS `property_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Izbacivanje podataka za tablicu `property_rule`
--

INSERT INTO `property_rule` (`id`, `rule_id`, `property_id`, `created_at`, `updated_at`) VALUES
(3, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 6, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 7, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 11, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 12, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 10, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 8, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 9, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 13, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 14, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 15, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 16, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 7, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 17, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Izbacivanje podataka za tablicu `rules`
--

INSERT INTO `rules` (`id`, `device_type`, `name`, `text`, `created_at`, `updated_at`) VALUES
(2, 'template', '', 'import org.openhab.core.library.types.*\nimport org.openhab.core.persistence.*\nimport org.openhab.model.script.actions.*    \n\nvar boolean power_enable = true\nvar Timer timer\n\n\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'heater', 'turn ON at', '\n\n//s4rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    if (power_enable == true  ){\n        |||item__switch|||.sendCommand(ON)\n        |||item__set_temperature|||.sendCommand(|||property__4|||)\n    }\nend\n//e4rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'light', 'turn ON at', '\n//s6rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    if (power_enable == true  ){\n        |||item__switch|||.sendCommand(ON)\n    }\nend\n//e6rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'air_conditioner', 'turn ON at', '\n//s7rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    if (power_enable == true  ){\n        |||item__power|||.sendCommand(ON)\n        |||item__tempset|||.sendCommand(|||property__4|||)\n    }\nend\n//e7rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'heater', 'turn OFF at', '\n//s8rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    |||item__switch|||.sendCommand(OFF)\nend\n//e8rule\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'air_conditioner', 'turn OFF at', '\n//s9rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    |||item__power|||.sendCommand(OFF)\nend\n//e9rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'light', 'turn OFF at', '\n//s10rule\nrule "|||userRule__name|||"\nwhen\n    Time cron "0 |||property__3||| * * ?" \nthen\n    |||item__switch|||.sendCommand(OFF)\nend\n//e10rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'heater', 'turn OFF if room temperature is higher than ...', '\n//s11rule\nrule "|||userRule__name|||"\nwhen\n	Time cron "*/5 * * * * ?"\nthen\n	if(|||item__room_temperature|||.state > |||property__4|||){ \n             if(|||item__switch|||.state != OFF){\n                 |||item__switch|||.sendCommand(OFF)\n              }\n       }\nend\n//e11rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'air_conditioner', 'turn OFF if room temperature is lower than ...', '\n//s12rule\nrule "|||userRule__name|||"\nwhen\n	Time cron "*/5 * * * * ?"\nthen\n	if(|||item__tempin|||.state < |||property__4|||){ \n             if(|||item__power|||.state != OFF){\n                 |||item__power|||.sendCommand(OFF)\n              }\n       }\nend\n//e12rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'air_conditioner', 'turn OFF if window is open', '\n\n//s13rule\nrule "|||userRule__name|||"\nwhen\n   Item living_room_window__contact__contact changed \nthen \n     if (living_room_window__contact__contact.state == CLOSED) {	 \n        power_enable = false	 \n	 |||item__power|||.sendCommand(OFF)\n      }else{\n	power_enable = true  \n      } \nend\n//e13rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'heater', 'turn OFF if window is open', '\n\n//s14rule\nrule "|||userRule__name|||"\nwhen\n   Item living_room_window__contact__contact changed \nthen \n     if (living_room_window__contact__contact.state == CLOSED) {	 \n        power_enable = false	 \n	 |||item__switch|||.sendCommand(OFF)\n      }else{\n	power_enable = true  \n      } \nend\n//e14rule', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'heater', 'turn ON if room temperature is lower than ...', '\n//s15rule\nrule "|||userRule__name|||"\nwhen\n	Time cron "*/5 * * * * ?"\nthen\n	if(|||item__room_temperature|||.state < |||property__4|||){ \n             if(power_enable == true){\n                 if(|||item__switch|||.state != ON){\n                    |||item__switch|||.sendCommand(ON)\n                 }\n             }\n       }\nend\n//e15rule\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'air_conditioner', 'turn ON if room temperature is higher than ...', '\n//s16rule\nrule "|||userRule__name|||"\nwhen\n	Time cron "*/5 * * * * ?"\nthen\n	if(|||item__tempin|||.state > |||property__4|||){ \n             if(power_enable == true){\n                 if(|||item__power|||.state != ON){\n                    |||item__power|||.sendCommand(ON)\n                 }\n             }\n       }\nend\n//e16rule\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'light', 'turn OFF after ...', '\r\n//s17rule\r\nrule "|||userRule__name|||"\r\n    when\r\n        Item |||item__switch||| changed\r\n    then\r\n        if(|||item__switch|||.state == ON) {\r\n            timer = createTimer(now.plusSeconds(|||property__6|||)) [|\r\n                |||item__switch|||.sendCommand(OFF) \r\n            ]\r\n        } else {\r\n            if(timer!=null) {\r\n                timer.cancel\r\n                timer = null\r\n            }\r\n        }\r\n    end  \r\n//e17rule\r\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
